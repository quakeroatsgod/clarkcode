/*
Function name: LNR
Purpose: Prints out the contents of the tree in the order: left subtree - root node - right subtree
Input: node root - pointer to the root (or a current temporary node) of the BST.
Side effects: none
return: nothing because it is a void function. The contents of the current node are printed to stout however.
*/
#include "main.h"
void LNR(node root)	{
	if(root==NULL)	return;	//base case
	LNR(root->left);	//traverses the left-most subtree, prints the contents, and then traverses to the right subtree to print
	printf("Plate:<%s> Name: %s, %s\n", root->plate,root->last,root->first);	//those contents
	LNR(root->right);
}

