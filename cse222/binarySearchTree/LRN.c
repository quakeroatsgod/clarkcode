/*
Function name: LRN
Purpose: Prints out the contents of the tree in the order: left subtree - right subtree - root node
Input: node root - pointer to the root (or a current temporary node) of the BST.
Side effects: none
return: nothing because it is a void function. The contents of the current node are printed to stout however.
*/
#include "main.h"
void LRN(node root)	{
	if (root==NULL)	return;	//base case
	LRN(root->left);	//traverses the left subtree, then traverses the right subtree next
	LRN(root->right);
	printf("Plate: <%s> Name:%s, %s\n", root->plate, root->last, root->first);	//prints the contents of the current node.
}

