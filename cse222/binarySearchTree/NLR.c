/*
Function name: NLR
Purpose: Prints out the contents of the tree in the order: root node - left subtree - right subtree
Input: node root - pointer to the root (or a current temporary node) of the BST.
Side effects: none
return: nothing because it is a void function. The contents of the current node are printed to stout however.
*/
#include "main.h"
void NLR(node root)	{
	if(root==NULL)	return;
	printf("Plate: <%s> Name:%s, %s\n", root->plate, root->last, root->first);	//prints out the node first, and then
	NLR(root->left);				//traverses through the left subtree and then the right subtree
	NLR(root->right);
}

