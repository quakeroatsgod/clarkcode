/*
Function name: add
Purpose: Adds a licencse plate/first name/ last name to the binary search tree. The BST is keyed by licencse plate, using
standard lexicographic ordering as determined by strcmp

Input: node root - pointer to the root of the BST.
	char *plate - character array containing the license plate being added to the tree.
	char *first - first name associated with the plate.
	char *second - last name associated with the plate.
Side Effects:	Allocates space for a new node. Also dynamically allocates space for all three pieces of data.
		These must eventually be freed by the caller.
return: node root - the (potentially new) root of the tree after the above data has been added
*/
#include "main.h"
node add(node root, char *plate, char *first, char *last)	{
	if(root->plate==NULL)	{	//checks if the root node is empty;
		root->plate=malloc(strlen(plate)+1);	//allocates space for an array of characters that is the size o
		strcpy(root->plate,plate);		//of the license plate. The array is then copied into the root.
		root->first=malloc(strlen(first)+1);
		strcpy(root->first,first);		//Same thing for the firstname and lastname.
		root->last=malloc(strlen(last)+1);
		strcpy(root->last,last);
		root->left=NULL;
		root->right=NULL;
		return root;		//the root of the tree is then returned.
	}
	node newNode;		
	newNode=(struct node*)malloc(sizeof(struct node));	//allocates space for a new node
	if(newNode==NULL)	{
		printf("Out of space!");	//Edge case in case there is no more available memory.
		return root;	}
	newNode->plate=malloc(strlen(plate)+1);
        strcpy(newNode->plate,plate);		//Space is allocated for the plate, first, and last name just like the
       	newNode->first=malloc(strlen(first)+1);	//code above. The three values are then saved to the new node.
        strcpy(newNode->first,first);
        newNode->last=malloc(strlen(last)+1);
        strcpy(newNode->last,last);
	newNode->left=NULL;	//Initializes the new node to not point to anything. This prevents errors from occuring.
	newNode->right=NULL;	
	node previous;	
	node current=root;	//generates two temporary variables to traverse the tree.
	while(current != NULL)	{
		previous=current;	
		if(strcmp(current->plate, newNode->plate) > 0)	{ //compares the new plate and the current plate in the tree
			current=current->left;	} // if strcmp returns >0, then current moves to its right child node. If <0, then 
		else	{			//current moves to the left child. The process is repeated until current reaches an
			current=current->right;	}	//endpoint in the tree.
	}
	if(current==previous->left)	{	//Checks for whether current moved to the left or right. The parent node of current
		previous->left=newNode;	}	//then points to the new node.
	else	{
		previous->right=newNode;	}
	return root;		
}

