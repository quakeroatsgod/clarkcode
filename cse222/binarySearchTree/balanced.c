/*
Function name: balanced
Purpose:	checks for whether or not the tree is balanced. This means that the tree is of equal length on both sides of the root node.
		The height of the left subtree minus the height of the right subtree must also be greater than 1.

Input:	node root - pointer to root of the BST.

Side effects: The function acquires the height of the left and right subtrees in order to determine if the tree is balanced. If the root is NULL, then the ttree is automatically balanced.

Return: An integer, 1 for "success" and "0" for failure.
*/
#include "main.h"
int balanced(node root)	{
	if(root==NULL)	return (1);	//base case
	if( abs(height(root->left) - height(root->right)) > 1)	return 0; //checks if the absolute value of the left subtree height minus the right subtree
	if(balanced(root->left) && balanced(root->right))	return 1;	//height is greater than 1. If so, the balanced check fails.
	return 0;						//If both sides of a subtree is balanced, than a 1 is returned for success.
	//If one of the subtrees is not balanced, then then balanced check fails.
}

