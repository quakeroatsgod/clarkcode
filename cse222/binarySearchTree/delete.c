/*
Function name: delete
Purpose: removes a specified license plate node from the binary search tree. The tree is then resorted to adjust to the deleted node.
inputs: node root - pointer to root of the BST
	char *plate - name of the license plate that the user wants to delete
Side effects: This function frees up the memory allocated to the plate, and the first and last names associated with the plate.
return: the root of the BST.*/
#include "main.h"
node delete(node root, char *plate)	{
	if(root==NULL)  return 0; //base case
	node current=root;
	node previous, tmp;
	while(current != NULL)  {
		if(strcmp(current->plate, plate) == 0)  {	//searches for the location of the node to delete by comparing the license plates.
			break;	}
		previous=current;
		if(strcmp(current->plate, plate) > 0)   {
			current=current->left;  }
		else	{
			current=current->right; }
	}
	if(current==root)	{	//if the license to be deleted happens to be in the root node, then this if statement will search for the greatest
		node largest=root->left;	//license plate in the left subtree.
		while(largest->right != NULL)	{
			previous=largest;
			largest=largest->right;
		}
		node tmp=largest->left;
		if(tmp != NULL)	previous->right=tmp;//changes the parent of the largest node to point to the largest node's child
		root=largest;		//converts the largest node to be the root node.
		free(current->plate);
		free(current->first);	//frees up the old root node
		free(current->last);
		free(current);					//this function is incomplete
		return root;
	}
	if(current->right->plate != NULL && current->left->plate != NULL)	{
		previous=current->right;	}
	else	{
		previous->left=current->left;		}		
	free(current->plate); 
       	free(current->first);
        free(current->last);
	free(current);
	return root;
	
}	

