/*
Function name: height

purpose: Calculates the length of a subtree in the binary search tree. The length is the number of levels of nodes in a subtree.

input: node root - pointer to the root of the BST.

Side effects:	None

return: returns an integer dependent upon the calculated height of a subtree.
*/
#include "main.h"
int height(node root)	{
	if(root==NULL)	return (-1);	//base case
	int hleft=height(root->left);	//traverses the left node first in a subtree. The right node is then traversed until they reach the end of the tree.
	int hright=height(root->right);
	if( hleft > hright)	{	//compares the length of a left and right subtree. If the left side is larger, then the left height + 1 is returned
		return	(hleft+1);	}
	else	{			//if the right side is larger, then the right height + 1 is returned.
		return (hright+1);	}
}

