/*
Griffen Agnello		CSE 222		2/23/2021	PA4
This program generates a binary search tree using a database textfile consisting of license plates, first names, and last names. The program
first reads from the textfile and allocates memory for nodes in order to the license plate information. The license plates are sorted based on
how it compares with strcmp to other license plates. The program then prompts the user to enter a command or to search for a license plate.
the command "*DUMP" will print out the height of the tree, whether or not it's balanced, and the tree's contents in three visual methods.
The command "*DELETE (plate)" will delete the specified license plate from the tree. If the user presses CTRL + D, memory for the nodes is freed up
and the program exits.
*/
#include "main.h"
void main(int argc, char **argv)	{	//Pulls the database from the command line argument.
	if(argc!=2)	{
		printf("Usage: plate database_name\n");	//only 2 arguments (the database name) is allowed
		return;	}
	FILE *database;		//Allocates space to open and read the database.
	database=fopen(argv[1], "r");
	if(database==NULL)	{
		printf("Cannot open %s\n", argv[1]);	//this prints if the database cannot be opened.
		return;		}
//--- Threshold, after this line, the database can be accessed ----
	char input[50];
	char scanPlate[50], scanFirst[50], scanLast[50];	//Used for scanning
	struct node *root, *current;
	root= (struct node*)malloc(sizeof(struct node));	//Allocates space in memory for the root node; 
	root->plate=NULL;		
	while(NULL != fgets(input,50,database))	{		//Begins reading line after line from the database.
		sscanf(input,"%s%s%s", scanPlate, scanFirst, scanLast);	//each line is parsed to acquire the license plate, first, and last names
		if(root==NULL)	{		//If the root has not been filled yet, the first license plate and name goes into root.
			root= add(root, scanPlate, scanFirst, scanLast);}
		else	{
			add(root, scanPlate, scanFirst, scanLast);	} //all other additions do not require a variable to be done.
	}	
	//Threshold line, adding to the tree is completed and the program is ready to accept commands
	int decide, result;
	char deletePlate[50], userInput[50];
	char dump[]="*DUMP";
	char deleteCommand[]="*DELETE";
	while(1)	{
		decide=0;	//resets the decision variable
		printf("Enter a command or plate: ");	//prompts for a command or a license plate to search for
		if(NULL == fgets(userInput,40,stdin))		decide = 3;	//decide=3 when the user enters a CTRL+D
		sscanf(userInput,"%s%s",input,deletePlate);
		if(0 == strcmp(deleteCommand,input))	decide = 1;
		if(0 == strcmp(dump, input))		decide = 2;	//the input decides which switch statement to execute
		switch(decide)	{
			case 2:		printf("TREE HEIGHT:%d\nBALANCED:",height(root));
					if(balanced(root)==1)	{
						printf("YES\n");	}	//prints the height of the tree, if it is balanced, and the
					else	{				//contents of the tree in LNR form, NLR form, and LRN form.
						printf("NO\n");	}
					printf("\nLNR TRAVERSAL\n");
					LNR(root);
					printf("NLR TRAVERSAL\n");
					NLR(root);
					printf("LRN TRAVERSAL \n");
					LRN(root);
					break;

			case 1:		if(search(root, deletePlate) == 0)	{	//Checks if the plate is in the tree first.
						printf("Plate not found.\n");
						break;	}
					delete(root, deletePlate);	//Deletes the plate and frees its memory.
					printf("SUCCESS\n");
					break;

			case 3:		printf("\nexiting\n");
					treeFree(root->left);	//Frees up memory of the left subtree and then the right subtree.
					treeFree(root->right);
					free(root->plate);	//frees up the plate, firstname, and lastname memory locations for the root
					free(root->first);
					free(root->last);
					free(root);		//frees up the memory allocated to the root node.
					fclose(database);	//closes the database and exits the program
					return;
					break;

			default:	result=search(root, input);	//searches for the license plate in the tree.
					if(result == 0)	printf("Plate not found.\n");
					if(result == 1) 	{
						current=root;		//current starts at the root of the tree
						while(current != NULL)	{
							if(strcmp(current->plate, input) == 0)  {       //checks if the current plate and the searched plate are the same. If so, return a 1.
                        					break;       }
                					if(strcmp(current->plate, input) > 0)   {       //If strcmp returns >0, then the searched plate is to the left of the current node.
                       						 current=current->left;  }
 				               		else    {                                       //If <0, then the searched plate is to the right of the current node.
 				                	       current=current->right; }               //This process continues until the plate is found or if the current node reaches the
        					}
						printf("First name:%s\nLast name:%s\n",current->first,current->last);
					}
					break;
		}
	}
}

