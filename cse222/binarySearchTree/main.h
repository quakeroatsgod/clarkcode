#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct node{
	char *plate;
	char *first;
	char *last;
	struct node *left;
	struct node *right;
};
typedef struct node* node;

int height(node root);
int balanced(node root);
int search(node root, char *plate);
node add(node root, char *plate, char *first, char *last);
node delete(node root, char *plate);
void LNR(node root);
void NLR(node root);
void LRN(node root);
void treeFree(node root);


