/* 
Function name: search
purpose: Searches for a license plate that is in the binary search tree. 
Input: node root - pointer to the root node of the tree
	char *plate - The (assumed) license plate that the user is searching for.
Side Effects: The function needs to traverse the tree in order to find the license plate.
Return: an integer. A 1 means "success" and a 0 means "failure".
*/
#include "main.h"
int search(node root, char *plate)	{
	if(root==NULL)	return 0;	//base case
	node current=root;	//temporary node to traverse the tree. Starts from the root node.
	while(current != NULL)	{
		if(strcmp(current->plate, plate) == 0)	{	//checks if the current plate and the searched plate are the same. If so, return a 1.
			return 1;	}
		if(strcmp(current->plate, plate) > 0)   {	//If strcmp returns >0, then the searched plate is to the left of the current node.
			current=current->left;	}
		else	{					//If <0, then the searched plate is to the right of the current node.
			current=current->right;	}		//This process continues until the plate is found or if the current node reaches the 
	}							//end of the tree. A 0 is then returned signaling failure.
	return 0;	
}

