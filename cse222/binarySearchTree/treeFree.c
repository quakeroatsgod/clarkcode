/*
function name: treeFree
purpose: frees up the memory allocated for a node, license plate, first, and last name in a subtree of the binary search tree
input: node root - pointer to the root of the BST
side effects: none
returns: nothing since it is a void function. The function will end though when the memory allocated for a node is freed.
*/
#include "main.h"
void treeFree(node root)	{
	if (root==NULL) return; //base case
        treeFree(root->left);        //traverses the left subtree, then traverses the right subtree next
        treeFree(root->right);
	free(root->plate);
	free(root->first);
	free(root->last);
	free(root);	//frees the current node and all three of its contents from memory
}

