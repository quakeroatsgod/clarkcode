// header include
#include "header.h"

// global hashtablesize
int hashTableSize;

// hash init function
NODE** hashInit(int size) {
  // set global hash size var based on the value passed
  hashTableSize = size;

  // malloc space for table
  NODE** table = malloc(sizeof(NODE) * size);
  int i = 0;
  // loop and initialize a list at every index in the table
  while(i < size) {
    table[i] = listInit();
    i++;
  }
  // return pointer to table
  return table;
}

// hash function
int hash(char* plate) {
  // h for final hash and sum to hold onto sum
  // valgrind finally shut up
  int h = 0;
  int sum = 0;
  // loop through string
  for(int i = 0; plate[i] != '\0'; i++) {
    // add index + 5 * the ascii of character
    sum+=(i + 5) * plate[i];
  }
  // mod sum by hashtable size
  h = sum % hashTableSize;
  // return hash
  return h;
}
// free mem function
void hashFree(NODE** hashTable) {
  int i = 0;
  // loop through table and free each list
  while(i < hashTableSize) {
    listFree(hashTable[i]);
    i++;
  }
  // free full table afterward
  free(hashTable);
  return;
}
// hash find function
int hashFind(NODE** table, char* plate, char* first, char* last) {
  int i = 0;
  // loop through table
  while(i < hashTableSize) {
    // call list find with sentinal pointer and same parameters
    if(listFind(table[i], plate, first, last)) {
      return 1;
    }
    i++;
  }
  return 0;
}
// hash add function
void hashAdd(NODE** table, char* plate, char* first, char* last) {
  // hashs plate to find index to add to
  int loc = hash(plate);
  // adds to the list given a sentinel node
  listAdd(table[loc], plate, first, last);
  //return 
  return;
}
// hash load function
void hashLoad(NODE** hashTable) {
  int i = 0;
  // loop through table
  while(i < hashTableSize) {
    // print entry # and length from listLen of the index
    printf("Entry %i: length = %i\n", i, listLen(hashTable[i]));
    i++;
  }
  return;
}
// hash dump function
void hashDump(NODE** hashTable, int cell) {
  // if passed -1, dump all cells
  if(cell == -1) {
    int i = 0;
    while(i < hashTableSize) {
      // loop and run listPrint on all cells
      printf("Contents of cell %i\n", i);
      listPrint(hashTable[i]);
      i++;
    }
  } else {
    // if cell is anything but -1, just dump that cell
    printf("Contents of cell %i\n", cell);
    // call list print on just the one cell
    listPrint(hashTable[cell]);
  }
  return;
}

