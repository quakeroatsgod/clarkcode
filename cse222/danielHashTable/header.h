// main header for main file
#include "util.h"

// prototypes for functions
NODE** hashInit(int);

int hashFind(NODE**, char*, char*, char*);

void hashAdd(NODE**, char*, char*, char*);
void hashLoad(NODE**);
void hashDump(NODE**, int);
void hashFree(NODE**);

NODE* listInit(void);

void listAdd(NODE*, char*, char*, char*);
int listFind(NODE*, char*, char*, char*);
int listLen(NODE*);

void listPrint(NODE*);
void listFree(NODE*);

