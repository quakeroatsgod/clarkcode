// include for header file
#include "header.h"

// list initialize function
NODE* listInit(void) {
  // mallocs a sentinel node
  NODE* sent  = malloc(sizeof(NODE));
  // sets next to null to show this is the last/only node in the list
  sent->next  = NULL;
  // sets all fields to NULL to stop valgrind from complaining
  sent->plate = NULL;
  sent->first = NULL;
  sent->last  = NULL;
  // return pointer to sentinal
  return sent;
}

// list length function, returns length of list
int listLen(NODE* sent) {
  // if sentinal is only node in list, return 0;
  if(sent->next == NULL) { return 0; }

  // set i to 1 because if sentinal isn't only node in list then 
  // we have at least 1 more node 
  int i = 1;
  // create a first temp variable
  NODE* first = sent->next;
  // while first isn't null, increase i and then traverse list
  while(first->next != NULL) { i++; first = first->next; }
  // return i once out of loop
  return i;
}

// printing the list
void listPrint(NODE* sent) {
  // create a current node
  NODE* curr = sent->next;
  // loop while it isn't null
  while(curr != NULL) {
    // print fields of node in fancy format
    printf("Plate : <%s> Name: %s, %s\n", curr->plate, curr->last, curr->first);
    curr = curr->next;
  }
  // add line for readability
  printf("----------------------------------\n");
  
  return;
}
// list freeing function
void listFree(NODE* sent) {
  // temp next variable
  NODE* next = NULL;
  // if sentinal is only node, just free it and return
  if(sent->next == NULL) {
    free(sent);
    return;
  }
  // while sentinal isn't the only node
  while(sent != NULL) {
    // set next to sentinal's next
    next = sent->next;
    // free all the dynamically allocated fields
    free(sent->plate);
    free(sent->first);
    free(sent->last );
    // free sentinal after
    free(sent);
    // set sentinal to the next value and repeat until all nodes freed
    sent = next;
  }
  return;
}

// list find function
int listFind(NODE* sent, char* plate, char* first, char* last) {
  // sets curr to firs node
  NODE* curr = sent->next;
  // while it isn't NULL
  while(curr != NULL) {
    // if the plate matches
    if(strcmp(plate, curr->plate) == 0) {
      // copy first and last name to passed paramaters
      strcpy(first, curr->first);
      strcpy(last, curr->last);
      // return 1
      return 1;
    }
    curr = curr->next;
  }
  // return 0 if not found in list
  return 0;
}
// list add to add sentinal to list
void listAdd(NODE* sent, char* plate, char* first, char* last) {
  // create new node
  NODE* newn  = malloc(sizeof(NODE));
  // set next to whatever sentinal was pointing to
  newn->next  = sent->next;
  // make sentinal point at you
  sent->next  = newn;

  // malloc room for all the strings
  newn->plate = malloc(strlen(plate) + 1);
  newn->first = malloc(strlen(first) + 1);
  newn->last  = malloc(strlen(last ) + 1);

  // copy in all the strings
  strcpy(newn->plate, plate);
  strcpy(newn->first, first);
  strcpy(newn->last , last);

  //return 
  return;
}

