/*
Name......: Daniel Belousov
Course....: CSE 222
Date......: February 13th, 2020
Assignment: Plate PA3
Synopsis..: This is a C program that creates a hash table of linked lists
            to store a database of plate numbers and the owners first and last
            names. It then abstracts out the background stuff and allows just
            a few commands to be used to see inside the database and search
            for plates.
*/

// header to include for prototypes and node struct
#include "header.h"

// main with command line arguments
int main(int argc, char** argv) {
  // error out if there aren't 2 or 3 arguments because that's invalid 
  if(argc != 2 && argc != 3) {
    // tell them how to use the program
    printf("Usage: %s database [hashsize]\n", argv[0]);
    return 1;
  }
  // declare size as default 100
  int size = 100;
  // if hashtable size included argc would = 3
  if(argc == 3) {
    // parse command line to check if hashtable size if valid
    if(sscanf(argv[2], "%i", &size) != 1) {
      // error if not valid int
      printf("Usage: %s database [hashsize]\n", argv[0]);
      return 1;
    }
    // error if num is less than 1
    if(size < 1) {
      printf("ERROR: Hashsize should be at least 1\n");
      return 1;
    }
  }
  // open file
  FILE *FP = fopen(argv[1], "r");
  // check if file is invalid and error if so
  if(FP == NULL) {
    printf("ERROR: Cannot open %s\n", argv[1]);
    return 1;
  }
  // init hashTable
  NODE** table = hashInit(size);

  // temp char array for reading file line input
  char temp[256];

  // loop through file lines and stop if fgets returns null, aka file ends
  while(fgets(temp, 256, FP) != NULL) {
    // declaring temp variables for parsing the file
    char platet[32], firstt[32], lastt[32];
    // initializing for valgrind to shut up
    platet[0] = '\0';
    firstt[0] = '\0';
    lastt[0]  = '\0';
    // if sscanf doesn't return 3 then the file is somehow wrong
    if(sscanf(temp, "%s %s %s", platet, firstt, lastt) != 3) {
      printf("ERROR: file improperly formatted\n");
      return 1;
    }
    // add to hash
    hashAdd(table, platet, firstt, lastt);
  }
  // close file after I'm done with it
  fclose(FP);
  
  // declaring input buffer
  char input[120];

  // loop forever till broken from inside
  while(1) {
    // prompt
    printf("Enter command or plate: ");
    // if fgets = NULL then end it
    if(fgets(input, 120, stdin) == NULL) { break; }
    // cut off new line
    input[strlen(input) - 1] = '\0';
    // creating temp parsing variables
    char command[120];
    int arg;
    // using sscanf to separate command input
    int re = sscanf(input, "%s%i", command, &arg);
    // checking to see if command is *LOAD and running hashLoad and then going back to top
    if(strcmp(command, "*LOAD") == 0 && re == 1) { hashLoad(table); continue; }
    // checking to see if command *DUMP
    if(strcmp(command, "*DUMP") == 0) {
      // if command has no args then call hashDump with -1
      if(re == 1) {
        hashDump(table, -1);
      } else
      // if command has 1 arg and it's in the range of 0 and max size of hashTable
      // run with arg
      if(re == 2 && arg != -1 && arg < size) {
        hashDump(table, arg);
      } else {
        // error if it fails everything
        printf("ERROR: cell must be between 0 and %i\n", (size - 1)); 
      }
      // re-loop
      continue;
    }

    // if you're out of these then you're entering a plate

    // declaring first and last name char
    char firstf[32];
    char lastf[32];
    // calling hashfind
    if(hashFind(table, command, firstf, lastf)) {
      // if found print out first and last name and then loop again
      printf("First name: %s\nLast name: %s\n", firstf, lastf);
      continue;
    } else {
      // if not found, say and then loop
      printf("Plate not found\n");
      continue;
    }
  }
  // out here you're ending the program, free the memory
  printf("\nFreeing memory...\n");
  hashFree(table);
  return 0;
}

