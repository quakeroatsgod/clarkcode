// utility file for imports and node struct 
#ifndef UTIL_H
#define UTIL_H

// libraries to import
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//node struct
struct node {
  char* plate;
  char* first;
  char* last;
  struct node* next;
};

typedef struct node NODE;

#endif

