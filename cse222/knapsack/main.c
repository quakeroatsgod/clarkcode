/* Griffen Agnello	CSE222	3/4/21		PA5 Dynamic Programming
This program tackles the "knapsack problem". It accepts 2 arguments: an integer knapsack capacity and a database text file. Once the preliminaries are met, the program opens
the database and parses the item attributes from it. Then, the program calculates the maximum value (using the function maxVal) of all possible values (less than the total capacity).
The program then calculates the highest possible value using the total capacity. The highest possible value as well as the number of items to get that value are printed to stout. The file
is then closed and the program exits.
*/
#include "main.h"
struct attribute item[129];	//Global variables so that maxVal can use them. This one is for the items in the database.
struct returnedMaxVal saveMaxVal[1025];	//This one is for storing inventory and values returned by maxVal
void main(int argc, char** argv)	{
	int cap;
	FILE *database;
	if(argc != 3)	{
		printf("Usage: knapsack integer database.\n");	//exits if there is no database or capacity.
		return;	}
	database=fopen(argv[2], "r");
	if(database==NULL)	{	//Tries to open the database. Program exits if the database does not exist.
		printf("Cannot open %s\n", argv[2]);
		return;	}
	if(1 != sscanf(argv[1],"%d",&cap))	{	//Exits if the capacity is not an integer and or less than 0.
		printf("Knapsack capacity must be an integer.\n");
		return;	}
	if(cap < 0)	{
		printf("Knapsack capacity must be a postitive integer.\n");
		return;	}
//----------------- Done with preliminary. Ready to process
	char input[100], scanItem[100];
	int scanValue, scanWeight;
	int i=0;
	printf(" Name  Value  Weight\n---------------------\n");
	while(NULL != fgets(input,100,database))	{	//Scans line after line from the database. The contents of each line are stored as the items, values and weights.
		if(3 != sscanf(input, "%s%d%d", scanItem, &scanValue, &scanWeight))	{
			printf("Inappropriate data type(s) in the database\n");	//The parse must detect a (string integer integer) or else the program exits.
			return;	}
		item[i].value=scanValue;
		item[i].weight=scanWeight;
		strcpy(item[i].name,scanItem);		//Stores the attributes of each item into the item array.
		printf("  %s $%d %d\n", item[i].name, item[i].value, item[i].weight);
		i++;
	}
	printf("\nBag's capacity = %d\n", cap);
//Done putting items in array
	for(int k=0; k<cap; k++)  {
                for(int j=0; j<=i; j++)       {
			saveMaxVal[k].inventory[j]=0;	} //Initializes the inventory in each maxVal number to 0.
	}
	int temp=cap;	//Temporary variable equal to the capacity. Temp is decremented after each time through the loop.
	while(temp >=0)	{
		if(saveMaxVal[(cap-temp)].data == 0)	{//checks if nothing is saved to a certain maxVal index. If so, the corresponding maxVal value is saved to it.
		saveMaxVal[(cap-temp)] = maxVal((cap-temp),i);		}
	temp--;
	}
	struct returnedMaxVal bestAnswer = maxVal(cap,i); //Finds the highest possible value and prints it to stdout.
	printf("Highest Possible Value $%d\n",bestAnswer.data);
	for(int k=0; k<i; k++)	{	//prints the inventory of each item as long as there is at least 1 of it.
		if( saveMaxVal[cap].inventory[k] != 0)	printf("Number of %ss (Item %d): %d\n", item[k].name, k, saveMaxVal[cap].inventory[k]);
	}
	
	fclose(database);	//closes the database
} //done! :)


