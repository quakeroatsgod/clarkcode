/* Name: maxVal
Input:  -cap: the capacity of the knapsack
        -i: the total number items in the item array
Purpose: Calculates the highest value by taking the previous calculated max value and adding it to one of the item values.
Side effects: Increments the inventory of the corresponding item depending on what option is the maximum value.
Returns: a struct returnedMaxVal, meaning it returns the value obtained from maxVal as well as the inventory count.
*/
#include "main.h"
struct attribute item[129];     //Global variables so that maxVal can use them. This one is for the items in the database.
struct returnedMaxVal saveMaxVal[1025]; //This one is for storing inventory and values returned by maxVal

struct returnedMaxVal maxVal(int cap, int i)    {
        int option=0, flag=0;
        int itemNumber, maxValIndex;
        struct returnedMaxVal largest;
        largest.data=0;         //Initialized to 0 just in case no option is valid.
        for(int k=0; k<i; k++)  {
                largest.inventory[k]=0; //Intializes the inventory of every item in this struct to 0 (base case).
        }
        if(cap == 0)    return largest; //Base case if the current capacity is 0.
        for(int j=0; j < i; j++)        {       //for loop that runs through all of the items from the database.
                if(item[j].weight <= cap)       //if the item weight is less than the cap, then the max value is pulled from saveMaxVal.
                        option = item[j].value + saveMaxVal[(cap - item[j].weight)].data;//adds 1 of the item to the maxVal.
                        if(option > largest.data)       {       //Compares the current option to the largest value option.
                                largest.data = option;
                                flag=1;                 //When the flag is set, this means that one of the inventories is going to be incremented.
                                itemNumber=j;                           //itemNumber and maxValIndex are going to be used for this.
                                maxValIndex= cap - item[j].weight;
                        }
        }
        if(flag==1)     {
                for(int k=0; k<i; k++)  {
                        largest.inventory[k]=saveMaxVal[maxValIndex].inventory[k]; //copies the inventories of the previous max values into largest's inventories.
                }
                largest.inventory[itemNumber]=saveMaxVal[maxValIndex].inventory[itemNumber]+ 1; //increments the inventory of the item option of the highest value
        }
        return largest;
}

