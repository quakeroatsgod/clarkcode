#include "main.h"
/*
This function will insert a new number into the list. The list is then storted again so that the list is
from the smallest to the largest number.
*/
int add(struct node *ll, int dsiNum)	{	//accepts the current list and the user inputted number.
	int newNode = get_node(ll);	//Acquires the index for the next empty node. The index number is saved to newNode.
	int tmp;
	int previousNode=0;	//This is initialized to 0 so that it starts at the beginning of the list.
	if(newNode==MYNULL)     {		//This checks if the list is already full. If so, the number cannot be inserted. 
                printf("OUT OF SPACE\n");
                return 0;
        }
	ll[newNode].data = dsiNum;      //Saves the user's number to the available node that was found.
	int current = ll[previousNode].next; //Initialized to the node after the previous node.
	while(current != MYNULL && ll[current].data < dsiNum)	{	// This will move through the list until either
		previousNode = current;				//the user's number is smaller than a particular number in the list
		current = ll[current].next;			//or if the program reaches the end of the list ( largest number).
	}							//The program will then save the user's number right before the number
	tmp= ll[previousNode].next;				//larger than it in the list. If the user's number is the largest,
	ll[previousNode].next = newNode;			//then that number is saved at the end of the list.
	ll[newNode].next = tmp;
	printf("SUCCESS\n");
	return 1;
}

