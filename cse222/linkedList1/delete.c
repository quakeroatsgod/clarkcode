#include "main.h"
/*This program deletes the user's inputted number if it is in the list.
*/
void delete(struct node *ll, int dsiNum)	{	
	if(search(ll, dsiNum)	== 0)	{	//Checks if the number is in the list. If the number is
		printf("NODE NOT FOUND\n");	//not in the list, a message is displayed and the user is returned
		return;				//to the main function.
	}
	int previousNode = 0;		//initialized to 0 so the program starts looking from the start of the list.
	int current= ll[previousNode].next;	//initialized to begin right after the previous node.
	while(current != MYNULL && ll[current].data != dsiNum)	{	
		previousNode=current;		//Moves through the list until the number is found or if the program reaches the
		current=ll[current].next;	//end of the list.
	}
	ll[previousNode].next = ll[current].next;	//Adjusts the list to point to the next number in line.
	ll[current].next = MYNULL;			//For example, in the set {1,2,3} and 2 was deleted, the list would
	int index=current;				//now point from 1 -> 3.
	release_node(ll, index);			//The deleted node is reset so that another number may be put into it.
	printf("SUCCESS\n");
	return;
}	

