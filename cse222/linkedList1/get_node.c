#include "main.h"
//This function finds the next available node that is not being used.
int get_node(struct node *ll)	{
	int i =1;	//Item [0] is reserved for the sentinel node. The first possible usable node is [1].
	while(i<100)	{
		if(ll[i].valid == 0)	{	//Runs through the list and checks to see if a node is being used. If
			ll[i].valid = 1;	//Valid equals 1, then the node is already being used. If valid equals
			return i;		//0, then that node is available. The empty node's index is returned.
		}
		i++;				//increments to the next node in the list.
	}
	return MYNULL;	//Returns a MYNULL if the list is completely full.
}	

