/* Griffen Agnello 	CSE 222		1/14/2021
This program is a linked list. The program begins by intializing a new, empty list. Then, the program asks the user on what they want
to do. The user can enter 5 different options: x, p, i#, d#, or s#, where # is some number. The response is parsed and then executes the 
corresponding function. "X" will exit the program, "i" will insert a number and then sort the list, "d" will delete a particular number,
"s" will search for a particular number, and "p" will print from smallest to largest all the numbers in the list. A message stating
"SUCCESS" means the function succeeded processing.
*/
#include "main.h"	//prototypes
int main ()	{
	struct node ll[100];	//generates the linked list and then sends it to init to be initialized
	init(ll);
	char input[30];
	char letter;
	char extra[30];
	int dsiNum, result;
	while(1==1)	{	//Infinite loop until the user intentionally exits the program
		while (1==1)	{	//Infinite loop until the user enters a correct response
			printf("What would you like to do?\n\n>");
			fgets(input,10,stdin);	
			result = sscanf(input, "%c%d%s", &letter,&dsiNum,&extra);	//Parses the user's reponse. The letter, number,
			if(1 == result)	{						//and extra characters are saved into the
				if(letter == 'p' || letter == 'x')	{		//appropriate variables.
					break;
				}
			}
			if(2 == result)       {		//checks whether the user entered a p/x or a d/i/s.
				if(letter == 'd' || letter == 'i' || letter == 's')	{
					break;
				}
			}
											//These only print if there is no correct
			printf(" i#- insert into list\n p - print current items in list\n");			//input detected.
			printf(" d# - delete a specific item from the list\n s# - search for a specific item in the list\n x - exit program\n");
		}
		if(letter == 'x')	{
                        exit (0);}
		if(letter == 'p')	{
                        print(ll);}		//executes the corresponding function to the letter.
		if(letter == 'i')	{
			add(ll, dsiNum);}
		if(letter == 'd')	{
			delete(ll, dsiNum);}
		if(letter == 's')	{
			result = search(ll, dsiNum);
			if(result == 1)	{		//If the search function returns a 1, then the number is in the list.
				printf("FOUND\n");}	//If a 0 is returned, then the number is not in the list.
			if(result == 0)	{
				printf("NOT FOUND\n");	}
		}
	}
	return 0;	
}	// :)

