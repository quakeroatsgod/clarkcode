struct node	{	//Initializes the nodes. The three data types must be in this particular order.
	int data;
	int next;	
	int valid;
};

#include <stdio.h>
#include <string.h>	//library inclusions.
#include <stdlib.h>
#define MYNULL 0
void init(struct node *ll);
int add(struct node *ll, int number);		//function prototypes
void print(struct node *ll);
void delete(struct node *ll, int number);
int search(struct node *ll, int number);
int get_node(struct node *ll);
void release_node(struct node *ll, int indexnumber);

