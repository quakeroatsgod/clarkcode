#include "main.h"
//This functions prints all the current numbers in the list from smallest to largest.
void print(struct node *ll)	{
	int current;
	current=ll[0].next; //Initializes so that it points to the next number in the list.
	printf("< ");
	while(current != MYNULL)	{
		printf("%d ", ll[current].data);	//prints all the numbers in the list until
		current = ll[current].next;	//the last node in the list points to MYNULL
	}
	printf(">\n");
	return;
}

