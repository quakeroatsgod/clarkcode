#include "main.h"
//this function sets the index of the deleted node to be 0. This means that the node can be used again for another number.
void release_node(struct node *ll, int index)	{
	ll[index].valid = 0;
}

