#include "main.h"
//This function checks if a number is already in the list.
int search(struct node* ll, int dsiNum){
	int current = ll[0].next;	// initialized to point to the next node in the list.
	while(current != MYNULL && ll[current].data != dsiNum)	{
		current=ll[current].next;	//moves through the list until the number is found or if the end of
	}					//the list is reached.
	if(current == MYNULL)	{	//If the end of the list is reached, the function returns a 0 for "NOT FOUND"
		return 0;
	}
	return 1;	//if the number is found, a 1 is returned for "FOUND"
}	

