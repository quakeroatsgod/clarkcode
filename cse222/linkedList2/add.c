#include "main.h"
/* This function inserts a new number into the list if the number is not already there. The list is then storted again so that the list is
from the smallest to the largest number. The function accepts a pointer to the sentinel node as well as the inputted number. 
The function return value determines if the insertion was successful or not. */
int add(struct node *sentinel, int num)	{
	int result = search(sentinel,num);	//Checks if the inputted number is already in the list. If so, the number is not added again.
        if(result == 1) {
                return 2;
        }
        struct node *newNode = (struct node*)malloc(sizeof(struct node));	//Allocates memory to the new node.
        if(newNode == NULL)     {		//If the memory allocated is NULL, then there is no more usable memory. 
                return 0;}			//The insertion is then unsuccessful.
        newNode->data = num;			
        struct node *previous, *current;
        current = sentinel;			//The current node starts from the sentinel node. The current node will move
        while(current != NULL && current->data < num)   {	//through the list  until it is at the end or if it encounters
                previous = current;				//the first number that is bigger than the inputted number.
                current = current->next;
        }
								
        newNode->next = current;			//The new inputted number's node points to the current node, and the previous
        previous->next = newNode;			//node in the list points to the new node.
        return 1;
}
		
		

