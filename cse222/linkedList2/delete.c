/*This function deletes a specified number from the list. The function accepts a pointer to the sentinel node and the inputted number. 
This function can only run if the number is in the list. If so, the node is freed up from memory. The function should always be successful.*/ 
#include "main.h"
void delete(struct node *sentinel, int num)	{
	struct node *current, *previous;
	current = sentinel;		//The current node starts at the sentinel node. The node then runs through the list until
	while(current->data != num )	{	//it finds the specified number.
		previous = current;
		current = current->next;
	}
	previous->next = current->next;		//The previous node skips over the current node and points to node after current.
	current->next = NULL;			
	free(current);				//Frees up the memory allocated to the specified number
	return;
}


