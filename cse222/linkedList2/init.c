/*This function initializes the start of the linked list. The function returns the pointer to the sentinel node. 
The sentinel node is set to point to NULL initially. No parameters are required.*/
#include "main.h"
struct node *init()	{
	struct node *sentinel;	//initializes the sentinel node.
	sentinel=(struct node*)malloc(sizeof(struct node));	//Allocates memory to the sentinel node with the size
	sentinel->next=NULL;					//of a "struct node". The sentinel node points to NULL.
	return sentinel;
}

