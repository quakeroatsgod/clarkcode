/* Griffen Agnello      CSE 222         1/27/2021
This program is a linked list. The program begins by allocating memory for a new, empty list. Then, the program asks the user on what they want
to do. The user can enter 5 different options: x, p, i#, d#, or s#, where # is some number. The response is parsed and then executes the
corresponding function. "X" will exit the program, "i" will insert a number and then sort the list, "d" will delete a particular number,
"s" will search for a particular number, and "p" will print from smallest to largest of all the numbers in the list. A message stating
"SUCCESS" means the function succeeded processing.
*/
#include "main.h"
void main()	{
	struct node *sentinel;	//initializes the start of the list. The list is then allocated memory from the init function.
	sentinel = init();
	char input[3];
        char letter;
        char extra[30];
        int num, result;
        while(1==1)     {       //Infinite loop until the user intentionally exits the program
                while (1==1)    {       //Infinite loop until the user enters a correct response
                        printf("\n>");
                        fgets(input,10,stdin);
                        result = sscanf(input, "%c%d%s", &letter,&num,extra);       //Parses the user's reponse. The letter, number,
                        if(1 == result) {                                               //and extra characters are saved into the
                                if(letter == 'p' || letter == 'x')      {               //appropriate variables.
                                        break;
                                }
                        }
                        if(2 == result)       {         //checks whether the user entered a p/x or a d/i/s.
                                if(letter == 'd' || letter == 'i' || letter == 's')     {
                                        break;
                                }
                        }
                                                                                        //These only print if there is no correct
                        printf(" i#- insert into list\n p - print current items in list\n");                    //input detected.
                        printf(" d# - delete a specific item from the list\n s# - search for a specific item in the list\n x - exit program\n");
                }
                if(letter == 'x')       {
                        struct node *current;		//initializes a current node to start at the sentinel node. This node move through the
        		current = sentinel;		//list and continuously frees up the memory allocated to the nodes.
			while(current != NULL)	{	
				free(current);
                		current = current->next;
			}
			exit (0);}
                if(letter == 'p')       {
                        print(sentinel);}             //executes the corresponding function to the letter.
                if(letter == 'i')       {
                        switch (result=add(sentinel, num))	{
				case 0: printf("OUT OF MEMORY\n");	//The add function checks if the number is already in the list first.
					break;				//If it isn't, the number will either be added, or the system
				case 1: printf("SUCCESS\n");		//will run out of unused memory.
					break;
				case 2:	printf("NODE ALREADY IN LIST\n");
					break;
			}
		}
                if(letter == 'd')       {
			result = search(sentinel,num);
			if(result == 0)	{	
				printf("NODE NOT FOUND\n");	}	//The delete function will only run if the number is in the list.
			else	{					//The search function decides whether or not to procceed. 
                        	delete(sentinel, num);
				printf("SUCCESS\n");
				}
			}
                if(letter == 's')       {
                        result = search(sentinel, num);
                        if(result == 1) {               //If the search function returns a 1, then the number is in the list.
                                printf("FOUND\n");}     //If a 0 is returned, then the number is not in the list.
                        if(result == 0) {
                                printf("NOT FOUND\n");  }
                }
  	     }
}	// :)

