struct node	{		//declaration of the struct node. The variables must be in this order.
	int data;
	struct node *next;
};
#include <stdio.h>			//library inclusions
#include <stdlib.h>

struct node *init(void);			
int add(struct node *sentinel, int num);
void print(struct node *sentinel);		//prototypes
void delete(struct node *sentinel, int num);
int search(struct node *sentinel, int num);


