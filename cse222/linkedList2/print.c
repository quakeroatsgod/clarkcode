#include "main.h"
void print(struct node *sentinel)       {
        struct node *current = sentinel->next;
        while(current != NULL)    {
                printf("%d ", current->data);
                current = current->next;
        }
        printf("\n");
        return;
}

/*
******NOTE ON THE "Print Problem"*********
For some reason, when I try to compile the print function, I receive this error.

print.o: In function `print':
print.c:(.text+0x0): multiple definition of `print'
print.o:print.c:(.text+0x0): first defined here
collect2: error: ld returned 1 exit status
Makefile:2: recipe for target 'main' failed
make: *** [main] Error 1

I have researched this error extensively, and I cannot seem to fix it. I have tried every solution I saw that was
relevant to me. I tried everything I could think of. I moved the prototype and function definitions into different files.
I moved the struct node definition out of the header file. I moved around the compilations in the Makefile and so on.
I have spoken to a classmate that has already taken this class, and he saw nothing wrong with my code. I also copied
my prototype from PA 1 (which does work) and the problem was still prevalent.

I truly have no idea what the issue is. When the print function is commented out (as well as in the main func.), the
program works completely fine. I am also confident I wrote the print function correctly if it did work. I am please asking you
to not grade this issue too harshly.

*/

