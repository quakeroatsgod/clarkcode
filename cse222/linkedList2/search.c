/* This function determines whether or not a number is already in the list. It accepts the pointer to the sentinel node and the specified number.
The return value of the function tells if the function was or wasn't found.*/
#include "main.h"
int search(struct node *sentinel, int num)	{
	struct node *current;
	current = sentinel;		//The current node starts from the sentinel node. The node moves through the list until
	while(current!=NULL && current->data!= num)	{	//it reaches the specified number or the end of the list.
		current = current->next;
	}

	if(current==NULL) return 0;	//If the number is not found, a 0 is returned. If the number is found, a 1 is returned.
	return 1;
}	

