/*This function adds the user's number to the queue list. It accepts a pointer to the queue's sentinel and the user's number. The function allocates
storage for the number and then sends it to the tail of the queue.*/
#include "main.h"
void addQueue(struct node *queueSent, int num)	{
	struct node *newNode = (struct node*)malloc(sizeof(struct node));       //Allocates memory to the new node.
        if(newNode == NULL)     {               //If the memory allocated is NULL, then there is no more usable memory.
                printf("Out of space!\n");
                return ;}                      //The insertion is then unsuccessful.
        newNode->data = num;			
        if(queueSent->next == NULL)     {	//Places the new node directly after the sentinel node if there are no other nodes in the list.
                queueSent->next = newNode;
                newNode->next = NULL;
                return;
        }
	struct node* current = queueSent->next;	
	struct node* previous;
	while(current != NULL)	{
		previous = current;	//The current and previous nodes move through the list until they reach the end of it.
		current = current->next;
	}
	newNode->next = current;                        //The new node points to the current node, and the previous node points to the new node.
        previous->next = newNode;
}

