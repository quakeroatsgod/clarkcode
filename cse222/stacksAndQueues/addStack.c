/*This function add the user's number to the stack list. It accepts a pointer to the stack sentinel node as well as the user's number. Storage for
the new node is allocated. The node is then placed immediately after the sentinel node. The top of the stack always comes right after the sentinel node.*/
#include "main.h"
void addStack(struct node* stackSent, int num)	{
	struct node *newNode = (struct node*)malloc(sizeof(struct node));       //Allocates memory to the new node.
        if(newNode == NULL)     {               //If the memory allocated is NULL, then there is no more usable memory.
                printf("Out of space!\n");
		return ;}                      //The insertion is then unsuccessful.
	newNode->data = num;
	if(stackSent->next == NULL)	{	//Inserts the new node after the sentinel node if there are no other nodes in the list.
		stackSent->next = newNode;
		newNode->next = NULL;
		return;
	}
	newNode->next = stackSent->next;	//Sets the sentinel node to point to the new node, and the new node points to the
	stackSent->next = newNode;		//previous node that was the TOS.
	return;
}

