/*This function checks to see if the two lists are empty. It accepts pointers to the sentinel nodes of the queue and stack lists. It also accepts the current mode.
The mode is used to decipher which list needs to be checked. The function checks if the sentinels point to NULL. If so, then the lists are empty and an error message is displayed.*/
#include "main.h"
int errorMessage(struct node* stackSent, struct node* queueSent, int mode)	{
	if(mode==1)	{
		if(stackSent->next == NULL)	{	//Checks the stack list if mode is 1. A NULL will trigger the print statement.
			printf("Stack is empty!");
			return 1;}	
		else	{	
			return 0;}	//A 0 returned means the list has at least 1 number in it.
	}
	else	{
		if(queueSent->next == NULL)	{	//Same process for the queue list.
			printf("Queue is empty!");
			return 2;}
		else	{
			return 0;}
	}
}
	

