/*This function frees up the storage allocated to the nodes in both lists. The function accepts pointers to the sentinel nodes of the lists.
The function will then move through the lists and free up nodes until it reaches the end of the lists.*/
#include "main.h"
void freeNodes(struct node* queueSent, struct node* stackSent)  {
        struct node *current, *previous;	//The stack list is freed first.
        if(stackSent->next != NULL)	{	//The lists cannot be empty. Otherwise, the function will return abruptly. The sentinels are freed last.
		current= stackSent->next;	//Initializes previous and current to start at the first number in the list.
	        previous = stackSent;
	        while(current != NULL)  {	//Moves through the list until it reaches the end. The node previous to "current" is freed in memory.
	                previous = current;
			current= current->next;
	        	free(previous);
		}
	}

	if(queueSent->next != NULL)	{	//Same proccess for the queue list.
	        current = queueSent->next;
 	        previous = queueSent;
        	while(current != NULL)  {
                	previous = current;
			current = current->next;
	        	free(previous);
		}
	}
}

