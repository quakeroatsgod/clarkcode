/*
Griffen Agnello CSE222 2/10/21 PA3 Stack and Queue Lists. This program allows the user to store integers into either a stack list or a queue list, based on whatever mode is currently set.
The program begins in stack mode and initializes an empty stack list and queue list. The program then waits for a user input. Inputting a number will insert that number into
the current list. An "s" switches to the stack list and prints the stack. An "q" switches to the queue list and print the current queue. The lists will be printed if there is at least 1
number present in them. Inputting a "Q" will free up the nodes of the two lists and exit the program.
*/
#include "main.h"
void main()	{
	struct node *queueSent = init();	//Initializes pointers to the sentinel nodes of the queue list and stack list.
	struct node *stackSent = init();
	char input[10];
	int num, result, mode=1;	//Mode is set to 1 to signifiy that the list begins in stack mode. When mode = 0, the list is in queue mode.
	char letter;
	while(123)	{	//Infinite loop until the user exits the program
		while (1234)	{	//Infinite loop until a valid response is inputted.
			printf("\n>");
			fgets(input,10,stdin);
			result = sscanf(input, "%d", &num);
			if(result == 1)	{
				letter = 'a';	//If the user enters a number, a placeholder "a" as in "add" is saved to the letter variable.
				break;	}
			result = sscanf(input, "%c", &letter);	
			if(result == 1)	{		//Checks if the user entered one of the specified letters
				if(letter == 's' || letter == 'q' || letter == 'p' || letter == 'Q')	{
				break;	}
			}												
			printf("Usage:\n# - insert into stack or queue\ns - select STACK MODE and print stack\n");	//Help text
                        printf("q - select QUEUE MODE and print queue\np - remove top of stack / head of queue and print\nQ - quit program\n");
		}
		switch(letter)	{	//Switch statement to know what to do after a response is inputted
			case 'a': 	if(mode == 1)	{	//"a" will check what list is active, and then insert the user's number into the appropriate list.
					addStack(stackSent, num);	}	//Inserts into the stack list
					else	{
						addQueue(queueSent, num);	}	//inserts into the queue list
				break;
			case 's':	printf("Entering STACK MODE\n");	//Switches to the stack mode and prints the stack if is not empty.
					mode=1;
					if(errorMessage(stackSent, queueSent,mode)==1)  {	//If the list is empty, errorMessage returns a 1.
                                                        break;  }
					printStack(stackSent);
				break;
			case 'q':	printf("Entering QUEUE MODE\n");	//Switches to the queue mode and prints the stack if it is not empty.
					mode=0;
					if(errorMessage(stackSent, queueSent,mode)==2)  {
                                                        break;  }
					printQueue(queueSent);
				break;
			case 'p':	if(mode ==1)	{		//"Pops" the head of the queue / top of stack. In this context, the head/TOS is deleted from the list.
						if(errorMessage(stackSent, queueSent,mode)==1)	{	//errorMessage checks first if the lists are empty.
							break;	}
						printf("%d\n", popStack(stackSent));	}	//The number being deleted is printed to standard out.
					else	{
						if(errorMessage(stackSent, queueSent,mode)==2)	{
							break;	}
						printf("%d\n", popQueue(queueSent));	}
				break;
			case 'Q':	freeNodes(queueSent, stackSent);	//This frees up the allocated storage for the nodes. The sentinel nodes are freed last before exiting
					free(queueSent);			//The program.
					free(stackSent);
					exit(0);
				break;
		}
	}	
} //:)

