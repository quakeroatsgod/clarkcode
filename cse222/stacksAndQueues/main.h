#include <stdio.h>	//libraries are fun. It's been almost a year since I've been to the Clark library.
#include <stdlib.h>
struct node	{
	int data;			//Struct node declaration
	struct node* next;
};
struct node* init();
void addStack(struct node* stackSent, int num);
int popStack(struct node*);
void printStack(struct node* stackSent);
void addQueue(struct node* queueSent, int num);
int popQueue(struct node* queueSent);		//prototypes... or are they?
void printQueue(struct node* queueSent);
int errorMessage(struct node* queueSent, struct node* stackSent, int mode);
void freeNodes(struct node* queueSent, struct node* stackSent);

