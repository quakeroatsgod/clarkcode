/*This function pops the head of the queue. It accepts a pointer to the sentinel node of the queue list. The sentinel node is set to point
to the node after the head of the list. The head of the list is then freed from memory. The number that was contained in the deleted node is returned
to the main function. */
#include "main.h"
int popQueue(struct node *queueSent)	{
	struct node* current = queueSent->next;	
        struct node* deletedNode = current; //A copy of the head of the list is created so that it may be freed.
        int num = deletedNode->data;	//The data of the head is saved so that it may be returned later.
        current = current->next;	// The sentinel points to the node after the head of the list. The head of the list
        queueSent->next = current;	//is then freed from memory.
        free(deletedNode);
        return num;
}

