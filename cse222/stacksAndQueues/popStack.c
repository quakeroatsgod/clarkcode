/*This function pops the top of the stack list. The function accepts a pointer to the sentinel node of the stack list. The function then sets the sentinel node
to point to the node after the TOS. The TOS is then freed from memory. The number of the deleted node is then returned to the main function.*/
#include "main.h"
int popStack(struct node *stackSent)	{	
	struct node* current = stackSent->next;	
	struct node* deletedNode = current;//A copy of the head of the list is created so that it may be freed.
	int num = deletedNode->data; 	//The data of the head is saved so that it may be returned later.
	current = current->next;	// The sentinel points to the node after the head of the list. The head of the list
	stackSent->next = current; 	//is then freed from memory.
	free(deletedNode);
	return num;
}

