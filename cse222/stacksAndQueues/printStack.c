/*This function prints the contents of the queue list. It accepts a pointer to the sentinel node of the queue list. The function moves through the list
while printing out all the numbers in it until it reaches the end of the list. */
#include "main.h"
void printQueue(struct node* queueSent)	{
	struct node* current = queueSent->next;
	printf("HEAD --> ");
	while (current != NULL)	{	//Starts with the node after the sentinel node. The current node moves through the list and
		printf("%d ", current->data);	//prints the contents of each node.
		current=current->next;
	}
	printf("<-- TAIL");
}

