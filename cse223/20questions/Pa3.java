/* Griffen Agnello	CSE223		PA3 20 Questions Game	5/6/2021
This program loads in a database textfile and scans in each line. The lines are either labeled as a question or an answser. The Q/As are then saved into a binary tree.
The 20 questions game begins by asking the user to think of an object. The program will then continually ask yes or no questions from the database. If the program cannot guess the object,
the user will be asked to input a new question to the object. The question will be saved as a node into the tree. The Tree will also be written to the database textfile. Regardless if a new question
is written, another game will start again. In order to quit the game, the user must press ctrl-C. */
import java.util.Scanner;
import java.io.File;		//rare imports
class PA3{
	public static void main(String[] args)	{
		Scanner sc;		
		File database;
		if(args.length != 0)	{	//Checks if there is an argument from the command line
			try	{
				sc = new Scanner(new File(args[0])); //If so, the program tries to open and scan the file from the command line
				database = new File(args[0]); 	
				}
				catch (Exception CannotOpenFile){ //If the file cannot be opened, or does not exist, the program closes.
				System.out.println("Cannot open file <" + args[0] + ">.");
                	        return;	}
		}
		else	{
			try {		//If there is no command line agrument, the program tries to open specifically "20Q.txt".    
				sc = new Scanner(new File("20Q.txt"));
				database = new File("20Q.txt");
			}
			catch (Exception CannotOpenFile){
				System.out.println("Cannot open 20Q.txt."); 	//If the file cannot be opened, or does not exist, the program closes.
				return;
			}
		}
		System.out.println("20 Questions Game! Think of an object and I will try to guess what you're thinking of. Use y/n or yes/no.\n");	
		Tree tree = new Tree();	//Generates a new binary tree.
		tree.readDatabase(sc); //Scans in the database line by line and saves the lines to the tree.
		sc.close();		//Closes the scanner
		tree.play(database);	//Plays 20 questions until the user force closes the program.
		return;		//Did you see I mispelled "argument" before?
	}
}

