/*This class contains everything needed to save the text nodes and to play 20 Questions. This class uses nodes from the Node class in order
to store values and organize the tree. */
import java.util.Scanner;
import java.io.PrintWriter;
import java.io.File;
class Tree{
	Node root;		//When a tree is generated, a root node is used as the top of the tree. The root is the 1st question.
	public Tree()	{} 	//Constructs a new tree with absolutely nothing inside it... Does this means it's a hollow tree?

//------------Methods------------------
/*This method is named readDatabase. It accepts a scanner that scans the database file. It simply calls the ingest method with the scanner
as an argument. The root node is then linked to the tree */
	public void readDatabase(Scanner sc)	{
		root= ingest(sc);	//Calls ingest and saves the tree to the root node.
	}

/*This method is named ingest. It is private because it should only be called during readDatabase(sc). The database scanner is passed to this in order to generate nodes
into the tree. Each line from the database is scanned and determined to be either a question or an answer. A node is returned containing text as well as what the node points
to (if it is a question node)*/
	private Node ingest(Scanner sc)	{
		Node temp = new Node(); //creates a new node in memory
		String line="     ";
		while(sc.hasNext())    { //Loop that runs through the whole database file.
			if (sc.hasNext())    {
				line = sc.nextLine();	//reads the next line in the file
					if(line.charAt(0) == 'Q' && line.charAt(1)== ':'){
						temp.quora=false; 	//Checks if the line is a question. If so, the node's flag is set to a question (false)
						line = sc.nextLine();	//The next line is then saved into the node's text variable.
						temp.text = line;
						temp.yes = ingest(sc);	//The method recursively starts again. The idea is to format the questions and answers in NLR style
						temp.no = ingest(sc);
						return temp;		//Returns the current node in the subtree
					} 
					if(line.charAt(0) == 'A' && line.charAt(1)== ':')	{
						temp.quora=true;		//If the line is an answer, the node's text is saved and the node is returned. No 
						line = sc.nextLine();		//recursion happens here.
						temp.text = line;
						return temp;
					}
			}
		}
		return temp;		//returns the final node if there are no other lines to be read.
	}

/*This method is called play. It accepts the name of the database from a main class. It uses the tree that has been created to play 20 questions. The root question will
be asked, and depending on the user's response, the tree will be traversed to either the yes or no subtrees. The game ends when either the program wins or loses. If the 
program loses, it will ask the user to input a new question into the database.*/
	public void play(File database)	{
		Node current = root;
		Node previous= root;	//two temporary nodes to traverse the tree. Starts from the root
		Scanner scanInput = new Scanner(System.in);	//scanner to get user input
		String input;
		while(true)	{ //Infinite loop. Only breaks by using ctrl-C
			if(current.quora==false)	{// If the current node's text is a question, the method asks it.
				System.out.println(current.text + "?");
				while(true)	{
					input = scanInput.nextLine();	//scans the user's response
					if(input.equals("y") || input.equals("yes") || input.equals("YEP"))	{
						previous = current;	//checks for a legal input. If yes, the current node moves to the yes subtree
						current = current.yes;
						break;
					}
					if(input.equals("n") || input.equals("no") || input.equals("NOPE"))	{
						previous = current;
						current = current.no; //If the response is no, the current node moves to the no subtree
						break; 
					}	
					else	{
							System.out.println("Try again, say 'yes' or 'no'");	}
				}
			}
			else	{		//If the current node is an answer, the appropriate message is displayed
				System.out.println("Okay, I think I got it! Is it " + current.text + "?");
				while(true)	{
					input = scanInput.nextLine();
					if(input.equals("y") || input.equals("yes") || input.equals("YEP"))     {
						System.out.println("Cool, let's play again. Press any key when you thought of something.");
						input = scanInput.nextLine(); //If the guess is correct, nothing really happens.
						current=root;			//The current node just resets to the root and the game begins again.
						break;
					}
					if(input.equals("n") || input.equals("no") || input.equals("NOPE"))      { //This is where the fun begins
						System.out.println("Darn, I couldn't guess it. What were you thinking of?");
						String answer = scanInput.nextLine(); //acquires the object and a corresponding question
						System.out.println("What should I ask in the future for your object?");
						String question = scanInput.nextLine();
						insertQuestion(answer, question, current); //calls insertQuestion to add it to the tree
						saveNewQuestion(root,database);	//rewrites the database file with the new question		
						current=root; //resets the current node to the root node and plays again
						break;
					}
				}
			}
		}
	}			

/*This method is called saveNewQuestion. It is private because it is only meant to be called in play(). It accepts the node root as well 
as the name of the database file. The database is opened up with a print writer. A separate method named writeLine recursively
writes to the file. */
	private void saveNewQuestion(Node root, File database)	{
		PrintWriter pw;
		try {	//creates a new print writer that is the database. The database should open. I would be surprised if it didn't.
			pw=new PrintWriter(database);	}
		catch	(Exception CannotOpenFile){
			System.out.println("How did the file not open? Huh?????????????????");
			return;
		}
		writeLine(root, pw);	//calls writeLine and starts at the root node. Begins copying over the tree to the textfile.
		pw.close();	//closes the print writer.
	}
	
/*This method is called insertQuestion. It is private becuase it is only meant to be called in play(). It accepts the current node and the user's
 generated question and answer. It changes the current node to become a question node (using the text supplied by the user). It also
creates 2 new nodes that the question node points to. The old answer that use to be in current is now placed in the current.no node.
The new, user supplied answer is placed in current.yes. */
	private void insertQuestion(String answer, String question, Node current)	{
		Node oldAnswer = new Node();
		Node newAnswer = new Node();
		Node copyCurrent= new Node();	
		copyCurrent.text = current.text;	//copies over the text before the current node's information gets overwritten
		current.text = question;	//new current node is turned into the new question
		current.quora = false;
		current.yes = newAnswer;	//current node now points to 2 answers
		current.no = oldAnswer;
		oldAnswer.text = copyCurrent.text;	//old answer from the old current is is current.no
		oldAnswer.quora = true;
		newAnswer.quora =  true;
		newAnswer.text = answer;		//the newly created answer is in current.yes	
		return;
	}

//This method is used nowhere in any of the final product code. I created it when I was testing if I made the tree correctly. I thought I would 
//keep this in here for fun. 	
/*	public void print(Node root)	{
		if(root == null) return;
		System.out.println(root.text);
		print(root.yes);
		print(root.no);
	} */
		
/*This method is called writeLine. It accepts the root node as well as the print writer made by saveNewQuestion. The purpose of this is
to recursively write the contents of the tree out to the given textfile.*/
	private void writeLine(Node root, PrintWriter pw)	{	
		if(root == null) return;	//base case
		if(root.quora  == false)	pw.write("Q:\n");//checks it the current node is a question or answer.
		if(root.quora == true)		pw.write("A:\n");//The following letter is written
		pw.write(root.text + "\n");	//writes the current node out to the file
		writeLine(root.yes,pw);		//traverses down the yes subtree
		writeLine(root.no,pw);		//traverses down the no subtree
	}
}

