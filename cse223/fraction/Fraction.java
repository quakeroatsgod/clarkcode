/*Griffen Agnello	CSE223		PA 1 Rational Number Calculator		4/15/21
This is a java class that can generate and perform arithmetic on fractions. The numerator and denominator are divided into their own separate variables. 
This class is capable of adding, subtract, multiplying, or dividing two fractions at a time. After one of those operations is complete, the newly solved fraction
is reduced down as simple of a form as it can be.  This is done by using Euclid's algorithm to find the greatest common divisor between the numerator and the denominator.
The GCD then divides the num and denom into the simplified form. In addition, the class also addresses negative signs in the num and denom and if the num or denom
is equal to 0. Last, the class can concatenate the fraction into a string or as a double-precision floating point number.
*/
class Fraction	{
	
	private int fractNum, fractDenom;	//two integers, fractNum will be the numerator, fractDenom is the denominator of a fraction.
//---------------------Constructors-----------------
	
	//This constructs and returns a fraction of (num / denom) where num and denom are user / main class inputted. It is public to other classes.
	public Fraction(int num, int denom)	{
		fractNum=num;
		fractDenom=denom;
	}
	//This constructs and returns a fraction of (num / 1 ). This constructor is called if no denominator is specified 
	public Fraction(int num)	{
		fractNum=num;
		fractDenom=1;
	}
	
//----------------------Methods---------------

/*This method is named sub. It accepts a fraction "n" in order to subtract from a particular fraction. It is public to other classes. 
The operation is as follows: a     c   ad - bc
			     -  -  - = --------
			     b     d     b*d              where c/d is "n"
After subtracting n from a fraction, the new fraction is simplified. The new fraction is then returned.
*/
	public Fraction sub(Fraction n)	{
		Fraction newNum = new Fraction(1);	//creates a placeholder fraction to return.
		newNum.fractNum = ((fractNum * n.fractDenom) - (fractDenom*n.fractNum)); // sets new numerator to ad - bc where fractNum/Denom = a/b and n.fractNum/Denom = c/d
		newNum.fractDenom = fractDenom * n.fractDenom; //sets new denominator to b*d
		newNum.reduce();	//The fraction is then reduced to a simplified form.
		return(newNum);		//The simplified fraction is returned
	}

	
/*This method is named add. It accepts a fraction "n" in order to add to a particular fraction. It is public to other classes.
The operation is a follows:  a     c   ad + bc
                             -  +  - = --------
                             b     d     b*d              where c/d is "n"
After adding n to a fraction, the new fraction is simplified. The new fraction is then returned.*/
	public Fraction add(Fraction n)	{
		Fraction newNum = new Fraction(1); //creates a placeholder fraction to return.
		newNum.fractNum = ((fractNum * n.fractDenom) + (fractDenom*n.fractNum)); // sets numerator to ad + bc where fractNum/Denom = a/b and n.fractNum/Denom = c/d
		newNum.fractDenom = fractDenom * n.fractDenom; //sets denominator to b*d
		newNum.reduce(); 	//The fraction is then reduced to a simplified form.
		return(newNum);		//The simplified fraction is returned
	}
	

/* This method is named mul. It accepts a fraction "n" in order to multiply it by another fraction. It is public to other classes. 
The operation is as follows: a     c      a*c
                             -  *  - = --------
                             b     d     b*d              where c/d is "n"
After multiplying n to a fraction, the new fraction is simplified. The new fraction is then returned.*/
	public Fraction mul(Fraction n)	{
		Fraction newNum = new Fraction(1);	//creates a placeholder fraction to return.
		newNum.fractNum= n.getNum() * fractNum; //Sets numerator to equal a * c where n.getNum() is c and fractNum is a
		newNum.fractDenom= n.getDenom() * fractDenom; //Sets denominator to equal b*d where n.getDenom() is d and fractDenom is b
		newNum.reduce(); 	//The fraction is then reduced to a simplified form.
		return(newNum);		//The simplified fraction is returned
	}
	

/* This method is named div. Itt accepts a fraction "n" in order to divide a particular fraction by "n". It is public to other classes.
The operation is as follows: a     c      a*d
                             -  /  - = --------
                             b     d     b*c              where c/d is "n"
After dividing a fraction by n, the new fraction is simplified. The new fraction is then returned.*/
	public Fraction div(Fraction n)	{
		Fraction newNum = new Fraction(1);//creates a placeholder fraction to return.
		newNum.fractNum= n.fractDenom * fractNum; //Sets the numerator to equal d * a where d=n.fractDenom and a=fractNum
		newNum.fractDenom= n.fractNum * fractDenom; //Sets the denominator to equal c * b where c = n.fractNum and b=fractDenom
		newNum.reduce(); 	//The fraction is then reduced to a simplified form.
		return(newNum);		//The simplified fraction is returned
	}
	

/*This method is named gcd. It accepts the numerator and denominator of a fraction. It first converts either number to be positive if needed. 
Depending on which of the two numbers are larger,  the larger forgoes modular divison by the smaller "larger (mod smaller)" Once all requirements are satisfied,
the greatest common divisor is returned as an integer. This method is private so it may only be used in the reduce() method*/
	private int gcd(int num, int denom)	{
		int mod;	//stores the modular division of the two numbers
		if(num < 0)	num = num*(-1);		//converts to positive numbers if needed
		if(denom < 0)	denom = denom*(-1);
		if(num > denom)	{			//if the numerator is greater than the denominator, then (numerator % denominator) is performed
			mod = num % denom;
			if(mod == 0)	{		//if the remainder is 0, then  the denominator is returned. If not, the greatest common divisor is
				return denom; }		//found again using the denominator and the remainder
			else {
				return(gcd(denom, mod));
			}
		}
		else {
			mod = denom % num;	//(denominator % numerator) if the denominator is greater than the numerator.
                        if(mod == 0)    {
                                return num; }	//Same idea as the code above, this time the numerator and remainder are being used.
                        else {
				return(gcd(num,mod));
                        }
                }
	}
	

/*This method is named reduce. It does not require parameters. It first finds the greatest common divisor of the numerator and denominator of a particular fraction.
The GCD then divides both the numerator and denominator into a simplified form. Next, the negative sign is moved / removed if needed. Last, the denominator is changed
to 1 if the numerator is a 0. This method changes the form of a particular fraction, and thus does not need to return anything. The method is private so that it cannot
be used outside of this class.*/
	private void reduce()	{
		int gcdNumber;	
		if(fractNum != 0 && fractDenom !=0)	{	//Checks if the num/denom are equal to 0. If neither, than the GCD is calculated.
			gcdNumber = gcd(fractNum, fractDenom);
			fractNum = fractNum / gcdNumber;	//Divides the num/denom by the GCD.
			fractDenom= fractDenom / gcdNumber;
		}
		if (fractNum < 0 && fractDenom < 0)	{	//Checks if the fraction is in the form -/-. (negative over negative). If so, the num/denom
			fractNum = fractNum * (-1);		//are multiplied by -1 to remove the negative sign.
			fractDenom = fractDenom * (-1);
		}
		if (fractNum > 0 && fractDenom < 0)     {	//Checks if the fraction is in the form +/-. If so, the num/denom are multiplied by -1 to move the sign
                        fractNum = fractNum * (-1);		//to the numerator (-/+)
                        fractDenom = fractDenom * (-1);
                }
		if (fractNum == 0 && fractDenom != 0)	fractDenom = 1;	//Checks if the numerator is 0. If so, and the denom isn't 0, then the denom is 1.
	}


/*This method is named toDouble. It does not require parameters. It converts a fraction object (I just realized that's what the terminology is) into a number of type double.
In case the fraction is not already simplified, it first is simplified. The double equivalent is then returned. The method is public for outside classes.*/
	public double toDouble()	{
		reduce();	//runs the reduce method to simplifiy the fraction object.
		if(fractDenom==0)	return(Double.NaN);	//If the denominator is 0, the number is undefined, and thus "NaN" is returned.
		return((double)fractNum / fractDenom);		//casts the equation (num/denom) to type double and returns the value.
	}


/*This method is name getNum. It simply accesses and returns the numerator integer of a fraction object. This is public so that other classes, such as the main class, may 
access the value of the numerator.*/
	public int getNum()	{
		return(fractNum);
	}

/*This method is name getDenom. It simply accesses and returns the denominator integer of a fraction object. This is public so that other classes, such as the main class, may
access the value of the denominator.*/
	public int getDenom()	{
		return(fractDenom);
	}

/*This method is name toString. It It does not require parameters. It converts a fraction object into a string. In case the fraction is not already simplified, it first is simplified.
The string equivalent is then returned. The method is public for outside classes.*/
	public String toString()	{
		reduce();		//runs the reduce method to simplifiy the fraction object.
		if(fractDenom ==1)	return("" + fractNum);	//Only the numerator is converted to a string if the denominator = 1
		if(fractDenom == 0)	return("NaN");		//If the number is undefined, a "NaN" is returned.
		return("" + fractNum + "/" + fractDenom);	//base case for "normal" fractions
	}
	
}
		


