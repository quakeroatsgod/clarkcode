class Main{
	public static void main(String[] args)	{		
		Fraction neghalf=new Fraction(-1,-2);
		System.out.println("print out -1/-2 with getNum and getDenom half= " + neghalf.getNum() + "/" + neghalf.getDenom());
		neghalf=neghalf.mul(new Fraction(-4));
		System.out.println("multiply by -4 = " + neghalf.getNum() + "/" + neghalf.getDenom());
		neghalf=neghalf.mul(new Fraction(0));
		System.out.println("mult by zero= " + neghalf.getNum() + "/" + neghalf.getDenom());
		neghalf = new Fraction(8);
		System.out.println("reset num to 8 " + neghalf.getNum() + "/" + neghalf.getDenom());
		neghalf=neghalf.div(new Fraction(2));
		System.out.println("divide by two= " + neghalf.getNum() + "/" + neghalf.getDenom());
		neghalf=neghalf.div(new Fraction(0));
                System.out.println("divide by zero= " + neghalf.getNum() + "/" + neghalf.getDenom());
		System.out.println("New number! 10/3");
		Fraction three = new Fraction(10,3);
		three=three.add(new Fraction(1,2));
		System.out.println("add by 1/2= " + three.getNum() + "/" + three.getDenom());
		three=three.sub(new Fraction(1,2));
                System.out.println("subtract by 1/2= " + three.getNum() + "/" + three.getDenom());
		System.out.println("Making 10/3 a double = " + three.toDouble());
		System.out.println("printing 10/3 with toString = " + three.toString());
		Fraction four = new Fraction(10,0);
		System.out.println("printing undefined number = " + four.toString());
		Fraction five = new Fraction(6, 3);
		System.out.println("printout out 6/3 with toString, should be reduced to print out just a 2 = " + five.toString());
		System.out.println("printout out 6/3 with toDouble , should be reduced to print out 2.0000 = " + five.toDouble());
		Fraction six = new Fraction(5,-3);
		System.out.println("print 5/-3 with toString, = "+ six.toString());
		System.out.println("print 5/-3 with toDouble, = " + six.toDouble());
		
/*		Fraction x=new Fraction(2,3);
		Fraction power=new Fraction(1);
		for (int i=0; i<10;i++)	{
			System.out.println("("+x+")^"+i+"="+power.toDouble());
			power=power.mul(x);
		}
	//can nest stuff too
	System.out.println("1/3-1/5 + 1/7=" +
		(((new Fraction(1,3)).sub(new Fraction(1,5))).add(new Fraction(1,7)).toDouble()));
	
	*/}

}

