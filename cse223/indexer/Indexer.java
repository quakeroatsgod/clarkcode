/* Griffen Agnello 	CSE223		PA2 Procsessing text file with hash map		4/22/2021
This class accepts a textfile and processes it to generate a hash map. Unique words in the text file are used as the keys of the hash table. A "word" is any string of letters separated by spaces 
that only contains uppercase letters. Each location of a word is saved as an instance. These instances are then saved to a linked list that corresponds to the appropriate hash key.
From a main class, there are a few methods that may be called. The total number of instances of a word, the location of the nth instance of a word, and the total number of unique words may be 
found using the appropriate methods. The hash table may also be converted to a string.
*/
import java.util.Scanner;
import java.util.HashMap;
import java.util.LinkedList;
import java.io.File;

class Indexer	{
	
	private HashMap<String,LinkedList<Integer>> map = new HashMap<String,LinkedList<Integer>>(); //Global object that creates a new, empty hash table whose values are integer linked lists.
	private boolean processFlag=false; //This is a flag to determine if a text file has been processed or not.
//---------Constructor------------
	public Indexer()	{	//This is used to construct indexers in the main class.
	}

//---------Methods--------------
/* This is a public method named processFile. It accepts a text file that will be processed. The method first tries to open the file. If it succeeds, the method will scan each word, 
"clean up" each word, and then send the reference and current location to addReference so that they may be added to the hash map. Once the entire file has been processed, the file is closed
and a "true" is returned. This true means that the file has been processed.*/
	public boolean processFile(String filename){
		Scanner sc; 	//Initializes a scanner to read the file.
		String word;
		int location=1;	//Location 1 corresponds to the first word in a text file. I should note that the example PA2Test output is the same as mine when loc=1. When loc=0 all the numbers 
				//are offset by 1. The directions say to use loc=0... Just informing
		try	{		//Tries to open and scan the text file. 
			sc = new Scanner(new File((filename)));	}
		catch (Exception CannotOpenFile){	//If unsuccessful, the exception CannotOpenFile is thrown, the process flag is changed to "false", and the file has been failed to process.
			processFlag=false;			
			return (false);	}
		processFlag=true;	//The flag is set to true if the try statement was successful.
		while (sc.hasNext())	{	//while loop that iterates through every word in the textfile as long as there is another word to read.
			word = sc.next();	
			word = cleanUp(word); 	//calls the cleanUp method to change the letters to uppercase and to remove any non-alphabetical characters.
			if(word.equals("") == false){	//In case a word is completely deleted by cleanUp, the entry ""  (blank) will not be added to the hash table.
				addReference(word, location);	//calls the addReference method to generate a hash map entry with the current word and location
				location++;}	
		}
		sc.close();	//closes the file after reading evvery line.
		return (true); //returns true to indicate that the file has been processed.
	}

/*This private method is named addReference. It accepts a string word as well as a location integer in a textfile. It generates an entry to the hash map with the word as the key and a linked list 
as the value. The linked list contains the location of an instance of a word. If a linked list is already established for a word, the location number is added to the list. Nothing is returned
since the hashmap is a global variable. */	
	private void addReference(String word, int location)	{
		if(processFlag==false) return; //nothing is added if the file has not been processed yet.
		LinkedList<Integer> ll;		//initializes a linked list of integers object 
		if(map.containsKey(word) == false)	{ //checks if the key word is already in the hash map. If not, a new entry is made
			ll = new LinkedList<Integer>();	//creates a linked list to hold the locations for a word
			ll.add(location);		//appends the first location to the list
			map.put(word, ll);		//adds the entry to the hash map
		}
		else{
			ll= map.get(word);	//If the key word is in the hash map, the corresponding linked list is pulled down.
			ll.add(location);	//Appends the location number to the list.
		}
		return;	
	}

/*This method is named numberOfInstances. It accepts a key word from the hash map. The method accesses the linked list associated with the hash map and returns the
total number of elements in the list. This number is the total number of times a particular word is in a textfile. */
	public int numberOfInstances(String word){
		if(processFlag==false) return(-1);	//A -1 is returned if the text file has not been processed yet.
		if(map.containsKey(word) == false)	return (0);	//If the word is not in the hash map, a 0 is returned.
		LinkedList<Integer> ll = map.get(word);	//Loads the linked list for the key word
		return(ll.size());	//returns the number of elements in the list
	}

/*This method is named locationOf. It accepts the nth instance integer that a word appears and a string word. The method accesses the linked list that corresponds with a key word. 
The nth instance of a word is used to return the location of the nth instance in a text file. */	
	public int locationOf(String word, int instanceNum)	{
		if(processFlag==false) return(-1); //A -1 is returned if the text file has not been processed yet.
		if(map.containsKey(word) == false)      return (-1);     //If the word is not in the hash map, a -1 is returned.	
		if(instanceNum < 0 || instanceNum >= numberOfInstances(word))	return(-1); //Return -1 if the instance number is less than 0 or greater than the total # of instances
		LinkedList<Integer> ll = map.get(word); //Loads the linked list for the key word
		return(ll.get(instanceNum));	//returns the location of the instance
	}
/*This method is named numberOfWords. Its has no parameters. It returns the total number of keys in the hash map to indicate the total number of unique words in the text file.*/	
	public int numberOfWords(){
		if(processFlag==false) return(-1);      //A -1 is returned if the text file has not been processed yet.
		return(map.size());	//Returns the total # of keys in the hash table.
	}
/*This method is named toString. It has no parameters. It returns a string of the hash table and its values. I saw no purpose to implement it in any method. I am assuming that was the idea
but I was still unsure. Regardless, this method works.*/
	public String toString(){
		if(processFlag==false) return(null); //A null is returned if the text file has not been processed yet.
		return(""+ map);	//Returns the map in the form of a string.
	}

/*This is a private method named cleanUp. It accepts a string (of 1 word) that is to be used as a key for the hash map. This method removes any characters that are not a letter
from the string. The string is then converted to all upper case letters. The resulting string is returned. */
	private String cleanUp(String word)	{
		String newWord="";	//blank string to add to
		for(int i=0; i<word.length();i++)	{	//this iterates through the length of the word.
			if(Character.isLetter(word.charAt(i)) == true)	{	//this decides if a particular character in the string is a letter.
				newWord= newWord + word.charAt(i);		//If so, the letter is appened to the newWord string.
			}
		}
		if(newWord.equals("") == true) return ("");
		newWord = newWord.toUpperCase(); //Converts the newWord string to uppercase. The resulting string is returned.
		return(newWord);	
	}
}

