import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

import javax.swing.JPanel;

/*
 *Names:
 *Nathan Spelts
 *Thomas Thanem
 *Logan McCartney
 */

@SuppressWarnings("serial")//hide our mistakes with a supress warning
public class MyPanel extends JPanel {
	//initialization of basic variables and linked lists.
	int cells = 8, margin = 20;
	private int x1 = 0, y1 = 0, x2 = 0, y2 = 0;//these are used with highlightline to be coordinate variables
	boolean initialized = false;
	boolean restart = false;
	String player1 = "1st Player", player2 = "2nd Player";
	LinkedList<Cell> cellArray = new LinkedList<Cell>();
	LinkedList<Cell> markedCells = new LinkedList<Cell>();
	
	//paint function :)
	public void paint(Graphics g) {
		//super paint function :)  but it also lets us use the default paint function from swing
		super.paint(g);
		// this for loop draws 5 lines in order to create a 5px wide line at the provided coordinates 
		for (int i = 3; i <= 7; i++) {
			if ((x1 >= margin) && (x2 <= getWidth() - margin) && (y1 >= margin) && (y2 <= getHeight() - margin)) g.drawLine(x1 + i, y1 + i, x2 + i, y2 + i);
		}
		//set the background to dark grey
		g.setColor(Color.DARK_GRAY);
		//draw the dots in the given area with the given size
		for (int x = margin; x < getWidth(); x += cellSize()) {//bounds for x axis
			for (int y = margin; y < getHeight(); y += cellSize()) {//bounds for y axis
				g.fillOval(x, y, 10, 10);//draw dot 
			}
		}
		//if the cell is marked check for an owner and draw an initial in it because it is full
		for (int i = 0; i < markedCells.size(); i++) {//loop through all the marked cells
			if (markedCells.get(i).owner == 1) {//if it has an owner, draw initial
				g.drawString(String.valueOf(player1.charAt(0)), markedCells.get(i).westCoord + 8, markedCells.get(i).northCoord + 20);
			} else {
				g.drawString(String.valueOf(player2.charAt(0)), markedCells.get(i).westCoord + 8, markedCells.get(i).northCoord + 20);
			}
		}
		
		// uses two for loops one to loop through LL and one to draw 5 lines (in order for 5px wide lines)
		for (int i = 1; i < cellArray.size(); i++) {
			for (int j = 3; j <= 7; j++) {
				// draws the line stored at location i in LL
				if (cellArray.get(i).north) g.drawLine(cellArray.get(i).westCoord + j, cellArray.get(i).northCoord + j, cellArray.get(i).eastCoord + j, cellArray.get(i).northCoord + j);
				if (cellArray.get(i).south) g.drawLine(cellArray.get(i).westCoord + j, cellArray.get(i).southCoord + j, cellArray.get(i).eastCoord + j, cellArray.get(i).southCoord + j);
				if (cellArray.get(i).east) g.drawLine(cellArray.get(i).eastCoord + j, cellArray.get(i).northCoord + j, cellArray.get(i).eastCoord + j, cellArray.get(i).southCoord + j);
				if (cellArray.get(i).west) g.drawLine(cellArray.get(i).westCoord + j, cellArray.get(i).northCoord + j, cellArray.get(i).westCoord + j, cellArray.get(i).southCoord + j);
			}
		}
		
	}
	
	// this method is just to retrieve coordinates that need to be drawn
	public void highlightLine(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	
	// this method adds a cell to markedCells linked list, so we can write initials on completed cells, also records player names
	public void markCell(Cell cellToMark, String a, String b) {
		markedCells.push(cellToMark);
		player1 = a;
		player2 = b;
		
	}
	
	// this method creates the linked list when the game starts
	public void initializeLL() {
		for (int i = 0; i < (cells) * (cells) + 1; i++) {
			Cell cell = new Cell();
			cell.index = i;
			cell = getCoordinates(cell);
			cellArray.add(i, cell);
		}
	}
	
	// this method calculates the coordinates of a cell (it's corners) and records that data in the cell object 
	public Cell getCoordinates(Cell cell) {
		if (cell.index != 0) {
			cell.northCoord = margin + ((cell.index - 1) / cells) * cellSize();
			cell.southCoord = cell.northCoord + cellSize();
			
			cell.westCoord = margin + (((cell.index - 1) % cells) * cellSize());
			cell.eastCoord = cell.westCoord + cellSize();
		}
		return cell;
	}
	
	// this method returns the size of the cells in this specific game
	public int cellSize() {
		return((getWidth() - cells - (2 * margin)) / cells);
	}
	
	// this method figures out which cell was clicked in based off provided coordinates
	public Cell determineCurrentCell(int x, int y, boolean highlight) {
		
		// intialize variables
		int i = 0, j = 1;
		int northCoord, southCoord, westCoord, eastCoord, index = 0;
		Cell currentCell;
		
		// infinite loop (waits for break statement)
		while(true) {
			// tests to see if if the x coordinate is in between two calculated bounds
			if (((margin + (cellSize() * i)) < x) && (x < (margin + (cellSize() * j)))) {
				// if it is we take note of the west/east coordinate of this cell as well as it's index
				westCoord = (margin + (cellSize() * i));
				eastCoord = (margin + (cellSize() * j));
				index = j;
				break;
			// otherwise we look ad different bounds
			} else {
				i++;
				j++;
			}
		}
		// re-initialize i and j so we can check y coordinates
		i = 0;
		j = 1;
		// infinite loop (waits for break statement)
		while(true) {
			// checks to see if y coordinate is between two calculated bounds
			if (((margin + (cellSize() * i))< y) && (y < (margin + (cellSize() * j)))) {
				// if it is we mark the north/south bounds of this cell and it's index
				northCoord = (margin + (cellSize() * i));
				southCoord = (margin + (cellSize() * j));
				index += i * (cells);
				break;
			// otherwise we change the bounds we're checking
			} else {
				i++;
				j++;
			}
		}
		// checks to see if the index is within the margins
		if ((index <= (cells) * (cells))) {
			// creates new cell
			currentCell = new Cell();
			// stores the index, and coordinates in cell object
			currentCell.index = index;
			currentCell.northCoord = northCoord;
			currentCell.southCoord = southCoord;
			currentCell.eastCoord = eastCoord;
			currentCell.westCoord = westCoord;
			// returns cell object
			return currentCell;
		}
		// return null because index is outside of margins
		return null;
	}
	
	// this method cleans up LL's when game is restarted, as well as restarts the score
	public void restartGame() {
		
		cellArray.clear();
		markedCells.clear();
		NetDot.player1ScoreCount = 0;
		NetDot.player2ScoreCount = 0;
		initializeLL();
	}

}

