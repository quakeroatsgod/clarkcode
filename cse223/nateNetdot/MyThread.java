import java.net.Socket;
import java.util.Scanner;
import java.io.PrintWriter;

/*
 * Names:
 * Nathan Spelts
 * Logan McCartney
 * Thomas Thanem
 */ 

public class MyThread extends Thread {

	Scanner sc;
	PrintWriter pw;
	Socket sock;
	String coordinates = "";
	MyPanel playGround;
	String playerName;
	boolean sentName = false;
	boolean hasName = false;
		
	public MyThread(Socket sock) {
		this.sock = sock;
		
		try {
			// open pw if sending data, or open sc if receiving data
			pw = new PrintWriter(sock.getOutputStream());
			sc = new Scanner(sock.getInputStream());
		} catch (Exception e) {
			System.out.println("Error: " + e);
			return;
		}
	}
	
	// called when start is called
	public void run() {
		while (sc.hasNextLine()) {
			NetDot dots = NetDot.giveMeDaFrame();
			if (!hasName) {
				if (dots.server) {
					playerName = sc.nextLine();
					dots.player2field.setText(playerName);
					dots.player2Score.setText(playerName + "'s Score: 0");
				} else {
					playerName = sc.nextLine();
					dots.player1field.setText(playerName);
					dots.player1Score.setText(playerName + "'s Score: 0");
					dots.whoseTurn.setText(playerName + " goes first.");
				}
				hasName = true;
			}
			if (sc.hasNextLine()) coordinates = sc.nextLine();
			if (coordinates.length() > 0) {
				if (hasName && coordinates.substring(0, 1).equals("Q")) {
					pw.close();
					sc.close();
					dots.quitGame();
				} else {
					String[] moveData = coordinates.split(" ");
					Integer x = Integer.parseInt(moveData[0]);
					Integer y = Integer.parseInt(moveData[1]);
					dots.mouseClickLogic(x, y);
				}
			}
		}
	}
	
	public String receiveData() {
		// "to" flag is set to false, read from socket and send to stdout
		return coordinates;
	}
	
	public void send(String moveData) {
		if (!sentName) {
			pw.println(moveData);
			pw.flush();
			sentName = true;
			return;
		}
		pw.println(moveData);
		pw.flush();
		if (moveData.substring(0, 1).equals("Q")) {
			pw.close();
		}
		return;
	}
	
	public void giveMePlayGround(MyPanel playGround) {
		this.playGround = playGround;
	}
}

