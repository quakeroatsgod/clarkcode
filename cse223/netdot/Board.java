//CSE 223 PA4
//Created by: The Homies; Cyrus Santiago and Griffen Agnello
import java.util.LinkedList;

//Placeholder for Board sizing and setting
public class Board {
	int rowNumber = 8;
	int columnNumber = 8;
	Box Cell[][] = new Box[50][50];
	int margin = 10;
	int Col, Row;
	int cellHeight = (450 / (rowNumber + 1)); // Base case
	int cellWidth = (450 / (columnNumber + 1));
	String playerInitial;
	int r = 0, c = 0; // local variables to this class. Used for the index of Cell[c][r]

	public Board() {

	}

	// Builds a Cell from x,y info in MyPanel
	public void buildCell(int Col, int Row) {
		int xCoord = 17 + Col * cellWidth;
		int yCoord = 5 + Row * cellHeight;
		if (Row < 0) {
		}
		r = Row;
		c = Col;
		Cell[c][r] = new Box();
		Cell[c][r].topLine = yCoord; // sets topLine location
		Cell[c][r].bottomLine = yCoord + cellHeight; // sets bottomLine location
		Cell[c][r].leftLine = xCoord; // sets leftLine location
		Cell[c][r].rightLine = xCoord + cellWidth; // sets rightLine location
	}
	// Pulls row/column numbers from MyPanel class.
	public void acquireRowColumn(int newRowNumber, int newColumnNumber) {
		rowNumber = newRowNumber;
		columnNumber = newColumnNumber;
		cellHeight = (450 / (rowNumber + 1)); // Calculates new dimensions of boxes. 450 is the panel border.
												// RowNumber+1 is the number of dots
		cellWidth = (450 / (columnNumber + 1));
	}

	public int getRow(int x) {
		Row = x / cellHeight; /// Calculate Row
		return (Row);
	}

	public int getCol(int y) {
		Col = y / cellWidth; // Calculate Col
		return (Col);
	}
}


