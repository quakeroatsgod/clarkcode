import java.net.Socket;
import java.util.Scanner;
import java.net.ServerSocket;
import java.io.IOException;
import java.io.PrintWriter;

public class NetThread extends Thread{
	Socket sock;
	int xcoord=6, ycoord=8;
	int playerFlag;
	boolean clickReceived=false; 
	boolean	scanClickReceived=false;
	boolean toFlag;
	PrintWriter pw;
	Scanner sc;
	String lock="sdflkj";
	String coordsToString="false 0 0";
	String scan;
	NetDot dotFrame = NetDot.frame();
	
	public NetThread(Socket sock)	{
		this.lock=lock;
		this.sock=sock;
		this.toFlag=toFlag;
		try {
				pw=new PrintWriter(sock.getOutputStream());
		        sc=new Scanner(sock.getInputStream());
		} 
		catch (Exception e){
			System.out.println("Error: " + e);return;
		}
	}
	//it's entire purpose is to read, the bookworm	
	public void run()	{	//runs when start() is called in NetDot
		while (sc.hasNextLine())	{
			String[] tokens = (sc.nextLine()).split(" ");
			System.out.println("Token: "+tokens[0]);
			if(tokens[0].equals("true"))	{
				clickReceived=false;
				xcoord=Integer.parseInt(tokens[1]);
				ycoord=Integer.parseInt(tokens[2]);
				System.out.println("coords are updated: "+xcoord+" "+ycoord);
				dotFrame.updateGameBoard(xcoord,ycoord);
				dotFrame.setTurnLabel(true);
			}												
			if(tokens[0].equals("stop"))	{
				stopItGetSomeHelp(0);
				return;
			}
			if(clickReceived) {
				scanClickReceived=true;		
			}
		}		
		System.out.println("Exited while loop of sc "+toFlag+sock);
	}
	
			
void sendClick(int x, int y, Socket sock)	{
		if(x==8000)	{	//8000 is code for the quit button was just pressed.
			pw.write("stop 1 1");
			pw.flush();
			return;
		}
	 	xcoord = x;
		ycoord = y;
		clickReceived=true;
		pw.write(coordsToString());
		pw.flush();
		pw.close();
		System.out.println("Sent click as the "+ toFlag+ " "+ xcoord+ " " + ycoord);
		return;
	}
	
	private String coordsToString()	{
		coordsToString=""+clickReceived+ " " +xcoord+ " "+ycoord;
		String coords;
		coords=""+clickReceived+ " " +xcoord+ " "+ycoord;
		return coords;
	}
	
	public void stopItGetSomeHelp(int playerID)	{
			pw.close();
			sc.close();
			System.out.println("pw is closed. Pw id? "+pw);
			try {
				sock.close();
				System.out.println("sock is closed. sock id? "+sock);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		System.exit(0);
	}
}

