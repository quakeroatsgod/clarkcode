#include "main.h"
//This function receives user input on whether the guess was too high, too low, or correct.
char getAnswer() {
	char response[10];
	char answer;
	char extra[30];	//variable for storing extra inputs other than what is desired.
	while(1==1)	{
		printf("Please enter either an h, l, or c if the guess was high, low, or correct respectively.");
		fgets(response, 10, stdin);	//Asks for an input of exactly an h, l, or c.
		if (1 == sscanf(response, "%c%s", &answer,extra))	{	//The input then is scanned. If
			if(answer == 'h' || answer == 'l' || answer == 'c') {	//The input is invalid, the program
				break;						//will ask the user again for an input.
			}			
		}
	}
	return answer;	//Once a correct answer is detected, the letter is returned.
}
//***I experienced a bug that would print "Please enter either an h..." twice if the user did NOT exactly press enter
// during the intro function. If the user ONLY presses enter, the message is only displayed once as intended.
// To fix this bug, I tried...
//	setting the value of "str" in the intro function to NULL and Space after the fgets.
//	using scanf instead of fgets in the intro function.
//	using the memset function to set the value of "response" to 0 in order to "wipe" the variable value.
// No solution worked. Regardless, the game still functions fine with the bug, so I decided to move on from it.

