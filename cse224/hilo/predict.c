#include "main.h"
//This is the function that calculates the maximum number of guesses.
int predict(int low, int high) {	//The function will need the low and high values from the main function.
	double temp, prediction;	
	temp = (double) high - low + 1;		 //The algorithm subracts the low value from the high value, 
	prediction = (log(temp)) / (log( 2.0 )); //then adds 1 to it. This value is cast as the double data type.
	prediction++;				 //The new value from before is put into a base 10 logarithm
	return ((int)prediction);		 //divided by log base 10 of (2.0). The prediction is incremented
}						 //by 1 and then cast back and returned as an int.
	

