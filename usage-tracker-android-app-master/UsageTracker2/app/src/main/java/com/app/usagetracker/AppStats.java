package com.app.usagetracker;
import android.app.Service;
import android.app.usage.UsageStats;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import android.app.usage.UsageStatsManager;
import android.app.usage.UsageEvents;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
//import android.support.annotation.Nullable;
import java.security.Provider;
import java.util.Timer;
import java.util.TimerTask;
import java.util.List;
import android.os.Build;
import android.content.Intent;
import android.app.usage.UsageStats;
import android.widget.Toast;
//import android.support.annotation.RequiresApi;


public class AppStats extends Service {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public AppStats(){  //Constructor
    }

    public int onStartCommand(Intent intent, int flags, int startID) {
     //   Log.println(Log.INFO,null,"BEFORE BEFORE ANYTHING");
        sharedPreferences = getSharedPreferences("Usage Tracker",MODE_PRIVATE);
        editor=sharedPreferences.edit();
        TimerTask detectAppUsage = new TimerTask()    {
            @Override
            public void run()   {
             //   Log.println(Log.INFO,null,"BEFORE ANYTHING!");
                sharedPreferences = getSharedPreferences("Usage Tracker",MODE_PRIVATE);
                editor=sharedPreferences.edit();
                UsageStatsManager statManager = (UsageStatsManager)getSystemService(USAGE_STATS_SERVICE);
                long endTime= System.currentTimeMillis();
                long startTime=endTime-(1000);
                List<UsageStats> statUsageList= statManager.queryUsageStats(UsageStatsManager.INTERVAL_BEST,startTime,endTime);
              //  Log.println(Log.INFO,null,"BEFORE IF STATEMENT!");
                if  (statUsageList!=null){
                    for(UsageStats usageStat:statUsageList) {
                        //if(usageStat.getPackageName().toLowerCase().contains("clock")){
                        editor.putLong(usageStat.getPackageName(), usageStat.getTotalTimeInForeground());
                        //}
                        //else    {
                        //    Log.println(Log.INFO,null,"");
                        //    Log.println(Log.INFO,null,(usageStat.getPackageName()));
                        // }
                        // if(usageStat.getPackageName().toLowerCase().contain)
                        //     Log.println(Log.INFO,null,"SOMETHING HAPPENED");
                        // }
                        editor.commit();
                        editor.apply();
                    }

               // else    {
                 //   Log.println(Log.INFO,null,"NOPE 2");
                }

            }
        };
        Timer trackAppTime=new Timer();
        trackAppTime.scheduleAtFixedRate(detectAppUsage,0,1000);
        return super.onStartCommand(intent,flags,startID);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
