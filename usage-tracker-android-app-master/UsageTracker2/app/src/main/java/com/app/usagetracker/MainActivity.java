package com.app.usagetracker;
import android.app.AppOpsManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.util.List;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    public static String CLOCK_COUNTER="Clock Counter"; //key for usageStats
    public TextView fullReport_view;
    public static String clockTimeToString="";
    public static String fullReport="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences("Usage Tracker",MODE_PRIVATE);
        if(!isUsageAllowed())  {
            Intent usageAccessIntent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
            usageAccessIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(usageAccessIntent);
        //    Log.println(Log.INFO,null,"SUCCESS?");
            if(isUsageAllowed()){
         //       Log.println(Log.INFO,null,"INSIDE 2ND IF!");
                startService(new Intent(MainActivity.this,AppStats.class));
       //         Log.println(Log.INFO,null,"SUCCESS?");
            }
            else    {
                Toast.makeText(getApplicationContext(),"Requesting Access to stat tracking",Toast.LENGTH_SHORT).show();
            }
        }
        else    {
            startService(new Intent(MainActivity.this,AppStats.class));
          //  Log.println(Log.INFO,null,"2ND SUCCESS?!");
        }
        fullReport_view =findViewById(R.id.clock_time);
        long endTime= System.currentTimeMillis();
        long startTime=endTime-(1000);
        UsageStatsManager statManager = (UsageStatsManager)getSystemService(USAGE_STATS_SERVICE);
        List<UsageStats> statUsageList= statManager.queryUsageStats(UsageStatsManager.INTERVAL_BEST,startTime,endTime);
        TimerTask updateNewTime = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                  //      Log.println(Log.INFO,null,"INSIDE TIMERTASK RUN!");
                        long appTime=0;
                        long seconds;
                        long minutes;
                        long hours;
                        String appNameCutDown;
                        StringBuilder newLineOfApps= new StringBuilder();
                        for(UsageStats usageStat:statUsageList) {
                            //Log.println(Log.INFO,null,(usageStat.getPackageName()));
                            appTime=sharedPreferences.getLong(usageStat.getPackageName(), 0);
                            seconds = (appTime/1000)%60;
                            minutes=(appTime/(1000*60))%60;
                            hours =(appTime/(1000*60*60));
                            if(!(hours==0 && minutes==0 && seconds==0)) {    //If the time is not 0:0:0
                                appNameCutDown=usageStat.getPackageName();
                                if(appNameCutDown.contains("com.google.android."))     appNameCutDown=appNameCutDown.substring(19);
                                if(appNameCutDown.contains("com.google."))     appNameCutDown=appNameCutDown.substring(11);
                                if(appNameCutDown.contains("com.app."))     appNameCutDown=appNameCutDown.substring((8));
                                if(appNameCutDown.contains("com."))     appNameCutDown=appNameCutDown.substring(4);
                                if(appNameCutDown.contains("apps."))     appNameCutDown=appNameCutDown.substring((5));
                                newLineOfApps.append(appNameCutDown).append(" ").append(hours).append(" hr ").append(minutes).append(" min ").append(seconds).append(" sec").append("\n");
                            Log.println(Log.INFO,null,"blah"+appNameCutDown);
                            }
                        }
                        Log.println(Log.INFO,null,("After adding everything "+ newLineOfApps.toString()));
                        MainActivity.fullReport=newLineOfApps.toString();
                        fullReport_view.setText(newLineOfApps.toString());
              /*          long clock_time=sharedPreferences.getLong(CLOCK_COUNTER, 0);
                      //  String clock_timeToString="" + (sharedPreferences.getLong(CLOCK_COUNTER, 0));
                      //  Log.println(Log.INFO,null,(clock_timeToString));
                        long seconds = (clock_time/1000)%60;
                        long minutes=(clock_time/(1000*60))%60;
                         long hours =(clock_time/(1000*60*60));
                         MainActivity.clockTimeToString = "\n"+hours + ":"+ minutes+":"+seconds;
                        clock_view.setText(clockTimeToString);*/
                    //    Log.println(Log.INFO,null,"DONE WITH IMERTASK!");

                    }
                });
            }
        };
        Timer timer = new Timer();

        timer.scheduleAtFixedRate(updateNewTime,1,1000);
      //  Log.println(Log.INFO,null,"MADE TIMER AND TIMER.SCHEDULE...!");

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }

    public boolean isUsageAllowed(){
        try{
            PackageManager pkgManager = getPackageManager();    //Gets the package manager that holds all the apps
            ApplicationInfo appInfo = pkgManager.getApplicationInfo(getPackageName(),0);
            AppOpsManager appOpsManager = (AppOpsManager)getSystemService(APP_OPS_SERVICE);
            int mode= appOpsManager.unsafeCheckOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, appInfo.uid,appInfo.packageName);
         //   System.out.println("mode =" +mode+"          AppOpsManager.MODE_ALLOWED = "+ AppOpsManager.MODE_ALLOWED);
            if(mode== AppOpsManager.MODE_ALLOWED) {
                    return(true);
            }
            return false;
        }
        catch(Exception CannotFindStatManager)   {
            Toast.makeText(getApplicationContext(), "ERROR CANNOT FIND USAGE STATS MANAGER", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    protected void onDestroy() {
        if(isUsageAllowed()) {
            startService(new Intent(MainActivity.this, AppStats.class));
        }
        super.onDestroy();
    }
}