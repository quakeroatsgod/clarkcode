package com.app.usagetracker.ui.dashboard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.usagetracker.MainActivity;

public class DashboardViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<String> clockTime;
    public DashboardViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue(MainActivity.fullReport);
       // clockTime = new MutableLiveData<>();
      //  clockTime.setValue(MainActivity.clockTimeToString);
    }

    public LiveData<String> getText() {
        return mText;
    }
}